package it.unimib.hieraticgamehive.decorations;

import android.graphics.Rect;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class CoverImageItemDecoration extends RecyclerView.ItemDecoration {
    int leftMargin = 0;
    int spacing = 0;
    int rightMargin = 0;

    public CoverImageItemDecoration(int leftMargin, int spacing, int rightMargin) {
        this.leftMargin = leftMargin;
        this.spacing = spacing;
        this.rightMargin = rightMargin;
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        if (parent.getChildAdapterPosition(view) == 0) {
            outRect.left = leftMargin;
        }
        if (parent.getAdapter() != null && parent.getChildAdapterPosition(view) == parent.getAdapter().getItemCount() - 1) {
            outRect.right = rightMargin;
        } else {
            outRect.right = spacing;
        }
    }
}
