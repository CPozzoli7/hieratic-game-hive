package it.unimib.hieraticgamehive;

import androidx.annotation.Nullable;

import java.util.HashMap;
import java.util.Objects;

import proto.Game;

public class GameHashMap extends HashMap<Long, Game> {
    private final HashMap<Long, Integer> pointerCount = new HashMap<>();

    @Nullable
    @Override
    public Game put(Long key, Game value) {
        if (key == null || value == null) {
            return null;
        }
        Integer count = pointerCount.get(key);
        if (count == null) {
            pointerCount.put(key, 1);
            return super.put(key, value);
        } else {
            pointerCount.put(key, ++count);
            return value;
        }
    }

    public Game update(Long key, Game value) {
        boolean contains = super.containsKey(key);
        if (contains) {
            return super.put(key, value);
        }
        return null;
    }

    @Nullable
    @Override
    public Game remove(@Nullable Object key) {
        Integer count = pointerCount.get(key);
        if (count == null) {
            return null;
        } else if (count == 1) {
            pointerCount.remove(key);
            return super.remove(key);
        } else if (key.getClass().isAssignableFrom(Long.TYPE)) {
            pointerCount.put((Long) key, --count);
            return super.get(key);
        }
        return null;
    }

    @Override
    public boolean remove(@Nullable Object key, @Nullable Object value) {
        Integer count = pointerCount.get(key);
        if (count == null) {
            return false;
        } else if (count == 1) {
            boolean removed = super.remove(key, value);
            if (removed) {
                pointerCount.remove(key);
            }
            return removed;
        } else if (key.getClass().isAssignableFrom(Long.TYPE)) {
            boolean equals = Objects.equals(super.get(key), value);
            if (equals) {
                pointerCount.put((Long) key, --count);
            }
            return equals;
        }
        return false;
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException();
    }
}
