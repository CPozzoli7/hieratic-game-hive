package it.unimib.hieraticgamehive.core.interfaces;

public interface OnCompleteCallback<T> {
    void onComplete(T result);
    void onError();
}
