package it.unimib.hieraticgamehive.core;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import it.unimib.hieraticgamehive.models.CachedHighlightGameList;
import it.unimib.hieraticgamehive.models.Converters;
import it.unimib.hieraticgamehive.models.Token;
import it.unimib.hieraticgamehive.services.local.HighlightsDAO;
import it.unimib.hieraticgamehive.services.local.TokenDAO;

@Database(entities = {Token.class, CachedHighlightGameList.class}, version = 2)
@TypeConverters({Converters.class})
public abstract class MainDatabase extends RoomDatabase {
    abstract public TokenDAO getTokenDAO();

    abstract public HighlightsDAO getHighlightsDAO();

    private static MainDatabase INSTANCE;

    public static synchronized MainDatabase getDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context, MainDatabase.class, MainDatabase.class.getSimpleName())
                    .build();
        }
        return INSTANCE;
    }
}
