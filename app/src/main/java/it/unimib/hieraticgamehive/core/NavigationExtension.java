package it.unimib.hieraticgamehive.core;

import android.content.Intent;
import android.util.Log;
import android.util.SparseArray;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.navigation.NavController;
import androidx.navigation.NavHost;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.List;
import java.util.Objects;

import it.unimib.hieraticgamehive.R;
import it.unimib.hieraticgamehive.core.interfaces.ItemReselectedFragment;

public class NavigationExtension {
    private static final String TAG = "NavigationExtension";

    public static LiveData<NavController> setupWithNavController(
            BottomNavigationView bottomNavigationView,
            List<Integer> navGraphIds,
            FragmentManager fragmentManager,
            int containerId,
            Intent intent
    ) {
        SparseArray<String> graphIdToTagMap = new SparseArray<>();
        MutableLiveData<NavController> selectedNavController = new MutableLiveData<>();

        int firstFragmentGraphId = 0;

        for (int index = 0; index < navGraphIds.size(); index++) {
            int navGraphId = navGraphIds.get(index);
            String fragmentTag = getFragmentTag(index);

            NavHostFragment navHostFragment = obtainNavHostFragment(
                    fragmentManager,
                    fragmentTag,
                    navGraphId,
                    containerId
            );
            int graphId = navHostFragment.getNavController().getGraph().getId();
            if (index == 0) {
                firstFragmentGraphId = graphId;
            }

            graphIdToTagMap.put(graphId, fragmentTag);

            int selectedItemId = bottomNavigationView.getSelectedItemId();
            if (selectedItemId == graphId) {
                selectedNavController.setValue(navHostFragment.getNavController());
                attachNavHostFragment(fragmentManager, navHostFragment, index == 0);
            } else {
                detachNavHostFragment(fragmentManager, navHostFragment);
            }
        }

        int selectedItemId = bottomNavigationView.getSelectedItemId();
        final String[] selectedItemTag = {graphIdToTagMap.get(selectedItemId)};
        String firstFragmentTag = graphIdToTagMap.get(firstFragmentGraphId);
        final boolean[] isOnFirstFragment = {selectedItemTag[0].equals(firstFragmentTag)};


        bottomNavigationView.setOnNavigationItemSelectedListener(item -> {
            if (!fragmentManager.isStateSaved()) {
                String newlySelectedItemTag = graphIdToTagMap.get(item.getItemId());
                if (!selectedItemTag[0].equals(newlySelectedItemTag)) {
                    fragmentManager.popBackStack(firstFragmentTag, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    NavHostFragment selectedFragment = (NavHostFragment) fragmentManager.findFragmentByTag(newlySelectedItemTag);
                    if (selectedFragment != null) {

                        if (!firstFragmentTag.equals(newlySelectedItemTag)) {
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction()
                                    .setCustomAnimations(
                                            R.anim.nav_default_enter_anim,
                                            R.anim.nav_default_exit_anim,
                                            R.anim.nav_default_pop_enter_anim,
                                            R.anim.nav_default_pop_exit_anim
                                    )
                                    .attach(selectedFragment)
                                    .setPrimaryNavigationFragment(selectedFragment);
                            for (int i = 0; i < graphIdToTagMap.size(); i++) {
                                int key = graphIdToTagMap.keyAt(i);
                                String fragmentTag = graphIdToTagMap.get(key);
                                if (!fragmentTag.equals(newlySelectedItemTag)) {
                                    fragmentTransaction.detach(Objects.requireNonNull(fragmentManager.findFragmentByTag(firstFragmentTag)));
                                }
                            }
                            fragmentTransaction.addToBackStack(firstFragmentTag)
                                    .setReorderingAllowed(true)
                                    .commit();
                        }
                        selectedItemTag[0] = newlySelectedItemTag;
                        isOnFirstFragment[0] = selectedItemTag[0].equals(firstFragmentTag);
                        selectedNavController.setValue(selectedFragment.getNavController());
                        return true;
                    }
                }
            }
            return false;
        });

        setupItemReselected(bottomNavigationView, graphIdToTagMap, fragmentManager);
        setUpDeepLinks(bottomNavigationView, navGraphIds, fragmentManager, containerId, intent);

        int finalFirstFragmentGraphId = firstFragmentGraphId;
        fragmentManager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                if (!isOnFirstFragment[0] && !isOnBackStack(fragmentManager, firstFragmentTag)) {
                    bottomNavigationView.setSelectedItemId(finalFirstFragmentGraphId);
                }

                NavController controller = selectedNavController.getValue();
                if (controller != null && controller.getCurrentDestination() == null) {
                    controller.navigate(controller.getGraph().getId());
                }
            }
        });
        return selectedNavController;
    }

    private static void setUpDeepLinks(
            BottomNavigationView bottomNavigationView,
            List<Integer> navGraphIds,
            FragmentManager fragmentManager,
            int containerId,
            Intent intent
    ) {
        for (int index = 0; index < navGraphIds.size(); index++) {
            int navGraphId = navGraphIds.get(index);
            String fragmentTag = getFragmentTag(index);
            NavHostFragment navHostFragment = obtainNavHostFragment(
                    fragmentManager,
                    fragmentTag,
                    navGraphId,
                    containerId
            );
            if (navHostFragment.getNavController().handleDeepLink(intent)
                    && bottomNavigationView.getSelectedItemId() != navHostFragment.getNavController().getGraph().getId()) {
                bottomNavigationView.setSelectedItemId(navHostFragment.getNavController().getGraph().getId());
            }
        }
    }

    private static void setupItemReselected(
            BottomNavigationView bottomNavigationView,
            SparseArray<String> graphIdToTagMap,
            FragmentManager fragmentManager
    ) {
        bottomNavigationView.setOnNavigationItemReselectedListener(
                item -> {
                    String newlySelectedItemTag = graphIdToTagMap.get(item.getItemId());
                    NavHostFragment selectedFragment = (NavHostFragment) fragmentManager.findFragmentByTag(newlySelectedItemTag);
                    if (selectedFragment != null) {
                        NavController navController = selectedFragment.getNavController();
                        if (Objects.requireNonNull(navController.getCurrentDestination()).getId() == navController.getGraph().getStartDestination()) {
                            Log.d(TAG, "setupItemReselected: item reselected on start destination");
                            Fragment fragment = selectedFragment.getChildFragmentManager().getFragments().get(0);
                            if (fragment instanceof ItemReselectedFragment) {
                                ((ItemReselectedFragment) fragment).onItemReselected();
                            }
                        }
                        navController.popBackStack(navController.getGraph().getStartDestination(), false);
                    }
                }
        );
    }

    private static void detachNavHostFragment(
            FragmentManager fragmentManager,
            NavHostFragment navHostFragment
    ) {
        fragmentManager.beginTransaction().detach(navHostFragment).commitNow();
    }

    private static void attachNavHostFragment(
            FragmentManager fragmentManager,
            NavHostFragment navHostFragment,
            boolean isPrimaryNavFragment
    ) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction().attach(navHostFragment);
        if (isPrimaryNavFragment) {
            fragmentTransaction.setPrimaryNavigationFragment(navHostFragment);
        }
        fragmentTransaction.commitNow();
    }

    private static NavHostFragment obtainNavHostFragment(
            FragmentManager fragmentManager,
            String fragmentTag,
            int navGraphId,
            int containerId
    ) {
        Fragment existingFragment = fragmentManager.findFragmentByTag(fragmentTag);
        if (existingFragment != null) {
            return (NavHostFragment) existingFragment;
        }

        NavHostFragment navHostFragment = NavHostFragment.create(navGraphId);
        fragmentManager.beginTransaction()
                .add(containerId, navHostFragment, fragmentTag)
                .commitNow();
        return navHostFragment;
    }

    private static boolean isOnBackStack(
            FragmentManager fragmentManager,
            String backStackName
    ) {
        for (int i = 0; i < fragmentManager.getBackStackEntryCount(); i++) {
            if (backStackName != null && backStackName.equals(fragmentManager.getBackStackEntryAt(i).getName())) {
                return true;
            }
        }
        return false;
    }

    private static String getFragmentTag(int index) {
        return "bottomNavigation#" + index;
    }
}
