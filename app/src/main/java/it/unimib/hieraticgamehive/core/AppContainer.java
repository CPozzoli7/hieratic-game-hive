package it.unimib.hieraticgamehive.core;

import android.content.Context;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import it.unimib.hieraticgamehive.repositories.GameRepository;
import it.unimib.hieraticgamehive.repositories.UserRepository;
import it.unimib.hieraticgamehive.services.TokenManager;
import it.unimib.hieraticgamehive.services.remote.IGDBService;
import it.unimib.hieraticgamehive.services.remote.UserService;
import it.unimib.hieraticgamehive.viewmodels.factories.RepositoryViewModelFactory;

public class AppContainer {
    public MainDatabase mainDatabase;
    public TokenManager tokenManager;
    public IGDBService igdbService;
    public UserService userService;
    public RepositoryViewModelFactory repositoryViewModelFactory;
    public GameRepository gameRepository;
    public UserRepository userRepository;

    public AppContainer(Context context) {
        mainDatabase = MainDatabase.getDatabase(context);
        Executor executor = Executors.newCachedThreadPool();
        tokenManager = new TokenManager(mainDatabase.getTokenDAO(), executor);
        igdbService = new IGDBService(tokenManager);
        userService = new UserService();
        gameRepository = new GameRepository(igdbService, mainDatabase, executor);
        userRepository = new UserRepository(userService, executor);
        repositoryViewModelFactory = new RepositoryViewModelFactory(gameRepository, userRepository);
    }
}
