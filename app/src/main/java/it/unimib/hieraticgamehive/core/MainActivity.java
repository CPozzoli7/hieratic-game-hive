package it.unimib.hieraticgamehive.core;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.lifecycle.LiveData;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.preference.PreferenceManager;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import it.unimib.hieraticgamehive.R;
import it.unimib.hieraticgamehive.ui.fragments.GameDetailFragment;

public class MainActivity extends AppCompatActivity {
    private BottomNavigationView navView;
    private AppContainer appContainer;
    private LiveData<NavController> currentNavController = null;

    public AppContainer getAppContainer() {
        return appContainer;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.Theme_HieraticGameHive);

        appContainer = new AppContainer(getApplicationContext());

        SharedPreferences sharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(this);

        boolean automaticTheme = sharedPreferences.getBoolean("automatic_theme", true);
        boolean darkMode = sharedPreferences.getBoolean("dark_mode", true);
        if (!automaticTheme) {
            if (darkMode) {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            } else {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
            }
        }

        String defaultString = getResources().getString(R.string.language_default_entry);
        String language = sharedPreferences.getString("language", defaultString);
        Locale locale = new Locale(language);
        if (!language.equals(defaultString) &&
                !locale.getLanguage().equals(Resources.getSystem().getConfiguration().getLocales().get(0).getLanguage())) {
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.setLocale(locale);
            getResources().updateConfiguration(config, null);
        }

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
//        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
//                R.id.navigation_home, R.id.navigation_search, R.id.navigation_authentication)
//                .build();
//        NavHostFragment navHostFragment =((NavHostFragment)Objects.requireNonNull(getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment)));
//        NavController navController = navHostFragment.getNavController();
//        NavigationUI.setupWithNavController(navView, navController);

        if (savedInstanceState == null) {
            setupBottomNavigationBar();
        }

        handleIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        if (intent != null && intent.getData() != null) {
            String action = intent.getAction();
            if (!intent.getBooleanExtra("SOLVED", false) && action != null && action.equals(Intent.ACTION_VIEW)) {
                if (intent.getData().getScheme().equals("http") || intent.getData().getScheme().equals("https")) {
                    if (navView.getSelectedItemId() != R.id.search_navigation) {
                        navView.setSelectedItemId(R.id.search_navigation);
                    }
                    NavController navController = currentNavController.getValue();
                    String path = intent.getData().getEncodedPath();
                    if (navController != null) {
                        navView.post(() -> {
                            Bundle bundle = new Bundle();
                            bundle.putBoolean(GameDetailFragment.BOOLEAN_IS_INTENT_KEY, true);
                            bundle.putString(GameDetailFragment.STRING_GAME_URL_PATH_KEY, path.replace("/games/", ""));
                            navController.navigate(R.id.fragment_detail, bundle);
                        });
                    }
                } else if (intent.getData().getScheme().equals("app")) {
                    String fragment = intent.getData().getFragment();
                    if (fragment.equalsIgnoreCase("home")) {
                        if (navView.getSelectedItemId() != R.id.home_navigation) {
                            navView.setSelectedItemId(R.id.home_navigation);
                        }
                    } else if (fragment.equalsIgnoreCase("search")) {
                        if (navView.getSelectedItemId() != R.id.search_navigation) {
                            navView.setSelectedItemId(R.id.search_navigation);
                        }
                    } else if (fragment.equalsIgnoreCase("profile")) {
                        if (navView.getSelectedItemId() != R.id.profile_navigation) {
                            navView.setSelectedItemId(R.id.profile_navigation);
                        }
                    }
                }
            }
            intent.putExtra("SOLVED", true);
        }
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        setupBottomNavigationBar();
    }

    private void setupBottomNavigationBar() {
        navView = findViewById(R.id.nav_view);
        List<Integer> navGraphIDs = Arrays.asList(R.navigation.home_navigation, R.navigation.search_navigation, R.navigation.profile_navigation);
        currentNavController = NavigationExtension.setupWithNavController(navView, navGraphIDs, getSupportFragmentManager(), R.id.nav_host_fragment, getIntent());
    }

    @Override
    public boolean onSupportNavigateUp() {
        if (currentNavController == null) {
            return false;
        }
        if (currentNavController.getValue() == null) {
            return false;
        }
        return currentNavController.getValue().navigateUp();
    }

    public void navigate(@IdRes int destinationResId) {
        navView.setSelectedItemId(destinationResId);
    }

    private int savedPositionResId = 0;

    public void navigate(@IdRes int destinationResId, boolean saveCurrentPosition) {
        if (saveCurrentPosition) {
            savedPositionResId = navView.getSelectedItemId();
        }
        navigate(destinationResId);
    }

    public boolean navigateToSavedPosition() {
        boolean navigate = savedPositionResId != 0;
        if (navigate) {
            navigate(savedPositionResId);
        }
        resetSavedPosition();
        return navigate;
    }

    public void resetSavedPosition() {
        savedPositionResId = 0;
    }
}