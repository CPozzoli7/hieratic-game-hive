package it.unimib.hieraticgamehive.core.interfaces;

public interface ItemReselectedFragment {
    void onItemReselected();
}
