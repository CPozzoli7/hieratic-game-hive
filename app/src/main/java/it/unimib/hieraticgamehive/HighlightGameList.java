package it.unimib.hieraticgamehive;

import androidx.annotation.NonNull;

import java.util.List;

import it.unimib.hieraticgamehive.utils.Highlights;
import proto.Game;

public class HighlightGameList extends GameList {
    public final Highlights highlight;

    public HighlightGameList(GameHashMap gameHashMap, @NonNull List<Game> gameList, Highlights highlight) {
        super(gameHashMap, gameList);
        this.highlight = highlight;
    }
}
