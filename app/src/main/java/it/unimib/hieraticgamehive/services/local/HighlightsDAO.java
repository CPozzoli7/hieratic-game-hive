package it.unimib.hieraticgamehive.services.local;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import it.unimib.hieraticgamehive.models.CachedHighlightGameList;
import it.unimib.hieraticgamehive.utils.Highlights;

@Dao
public interface HighlightsDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(CachedHighlightGameList highlightGameList);

    @Delete
    void delete(CachedHighlightGameList highlightGameList);

    @Query("SELECT * FROM HIGHLIGHTS WHERE highlight = :highlight")
    CachedHighlightGameList findByHighlight(Highlights highlight);
}
