package it.unimib.hieraticgamehive.services.remote;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.JsonArray;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;

import it.unimib.hieraticgamehive.core.interfaces.OnCompleteCallback;

public class UserService {
    private static final String FIREBASE_USERS_PATH = "users";
    private static final String FIREBASE_COLLECTION_PATH = "collection";
    private static final String FIREBASE_WISHLIST_PATH = "wishlist";

    public enum UserEndpoint {
        COLLECTION(FIREBASE_COLLECTION_PATH),
        WISHLIST(FIREBASE_WISHLIST_PATH);

        private final String endpoint;

        UserEndpoint(String endpoint) {
            this.endpoint = endpoint;
        }

        public String getEndpoint() {
            return endpoint;
        }
    }

    public String getUid() {
        return FirebaseAuth.getInstance().getUid();
    }

    public FirebaseUser getUser() {
        return FirebaseAuth.getInstance().getCurrentUser();
    }

    public void getGameIds(
            Executor executor,
            UserEndpoint userEndpoint,
            OnCompleteCallback<List<Long>> onCompleteCallback
    ) {
        DatabaseReference firebase = getDatabaseReference(userEndpoint);
        if (firebase == null) {
            return;
        }
        firebase
                //.orderByChild("timestamp")
                .get()
                .addOnSuccessListener(executor, dataSnapshot -> {
                    List<Long> ids = new ArrayList<>();
                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                        Object value = child.child("id").getValue();
                        if (value != null) {
                            ids.add(Long.valueOf(value.toString()));
                        }
                    }
                    onCompleteCallback.onComplete(ids);
                }).addOnFailureListener(executor, e -> onCompleteCallback.onError());
    }

    public void addGameId(
            Long id,
            UserEndpoint userEndpoint
    ) {
        DatabaseReference firebase = getDatabaseReference(userEndpoint);
        if (firebase == null) {
            return;
        }
        DatabaseReference idReference = firebase.child(String.valueOf(id));
        idReference.child("id").setValue(id);
        idReference.child("timestamp").setValue(System.currentTimeMillis());
    }

    public void removeGameId(
            Long id,
            UserEndpoint userEndpoint
    ) {
        DatabaseReference firebase = getDatabaseReference(userEndpoint);
        if (firebase == null) {
            return;
        }
        firebase.child(String.valueOf(id)).removeValue();
    }

    private DatabaseReference getDatabaseReference(UserEndpoint userEndpoint) {
        if (getUid() == null) {
            return null;
        }
        return FirebaseDatabase
                .getInstance()
                .getReference()
                .child(FIREBASE_USERS_PATH)
                .child(getUid())
                .child(userEndpoint.getEndpoint());
    }
}
