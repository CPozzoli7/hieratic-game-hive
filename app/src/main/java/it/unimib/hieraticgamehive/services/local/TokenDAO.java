package it.unimib.hieraticgamehive.services.local;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import it.unimib.hieraticgamehive.models.Token;

@Dao
public interface TokenDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Token token);

    @Delete
    void delete(Token token);

    @Query("SELECT * FROM TOKEN")
    Token get();
}
