package it.unimib.hieraticgamehive.services;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.BuildConfig;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;

import java.util.Objects;
import java.util.concurrent.Executor;

import it.unimib.hieraticgamehive.core.MainDatabase;
import it.unimib.hieraticgamehive.core.interfaces.OnCompleteCallback;
import it.unimib.hieraticgamehive.models.Token;
import it.unimib.hieraticgamehive.services.local.TokenDAO;
import it.unimib.hieraticgamehive.utils.IGDBCredentials;

public class TokenManager {
    private static final String FIREBASE_CHILD_PATH = "credentials";
    private Token token;
    private final Executor executor;
    private final TokenDAO tokenDAO;

    public TokenManager(TokenDAO tokenDAO, Executor executor) {
        this.tokenDAO = tokenDAO;
        this.executor = executor;
    }

    /**
     * Grant access to the IGDB token through a callback using Firebase-rtdb and manage caching in local database.
     *
     * @param ignoreCache false to use cached token, true to ignore cached value, send a request to Firebase-rtdb and update cached value.
     *                    The token will be cached regardless this parameter.
     * @param callback    called once the token is available or on error.
     * @see MainDatabase
     * @see FirebaseDatabase
     */
    public synchronized void getToken(boolean ignoreCache, @NonNull final OnCompleteCallback<String> callback) {
        if (BuildConfig.DEBUG && IGDBCredentials.DEBUG_TOKEN != null && !IGDBCredentials.DEBUG_TOKEN.isEmpty()) {
            //Use DEBUG token if present
            callback.onComplete(IGDBCredentials.DEBUG_TOKEN);
        } else {
            //else, check token attribute, if not present read it (asynchronously) in local database
            executor.execute(() -> {
                if (token == null || ignoreCache) {
                    //Read token in local database
                    readToken(ignoreCache, callback);
                } else {
                    //If token attribute is populated return it using callback
                    callback.onComplete(token.accessToken);
                }
            });
        }
    }

    private static final int MAX_ERRORS = 3;
    private int errorCount = 0;

    private void readToken(boolean ignoreCache, @NonNull final OnCompleteCallback<String> callback) {
        //Use TokenDAO to access local database and get the token
        Token dbToken = tokenDAO.get();
        //If token is not present in local database, it is expired or cached value has to be ignored, send a request to Firebase-rtdb
        if (dbToken == null || !isValid(dbToken) || ignoreCache) {
            requestToken(dataSnapshot -> {
                //Get json string and parse it using Gson
                String value = String.valueOf(Objects.requireNonNull(dataSnapshot).getValue());
                Token taskToken = parseToken(value);
                if (taskToken != null) {
                    executor.execute(() -> callback.onComplete(taskToken.accessToken));
                    executor.execute(() -> setToken(taskToken));
                    executor.execute(() -> {
                        if (dbToken != null) {
                            //Update local db if token changed
                            if (!dbToken.equals(taskToken)) {
                                tokenDAO.delete(dbToken);
                                tokenDAO.insert(taskToken);
                            }
                        } else {
                            //Cache token in local db
                            tokenDAO.insert(taskToken);
                        }
                    });
                    errorCount = 0;
                } else {
                    callback.onError();
                }
            }, new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    if (++errorCount <= MAX_ERRORS) {
                        executor.execute(() -> readToken(ignoreCache, callback));
                    }
                    callback.onError();
                }
            });
        } else {
            executor.execute(() -> setToken(dbToken));
            executor.execute(() -> callback.onComplete(dbToken.accessToken));
        }
    }

    private void requestToken(OnSuccessListener<DataSnapshot> onCompleteListener, OnFailureListener onFailureListener) {
        //Get "credentials" json
        FirebaseDatabase
                .getInstance()
                .getReference()
                .child(FIREBASE_CHILD_PATH)
                .get()
                .addOnSuccessListener(onCompleteListener)
                .addOnFailureListener(onFailureListener);
    }

    private boolean isValid(Token token) {
        long currentTime = System.currentTimeMillis() / 1000;
        return currentTime < token.expiration;
    }

    private synchronized void setToken(Token token) {
        this.token = token;
    }

    private Token parseToken(String jsonString) {
        Gson gson = new Gson();
        return gson.fromJson(jsonString, Token.class);
    }
}
