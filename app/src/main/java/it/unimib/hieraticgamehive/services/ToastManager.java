package it.unimib.hieraticgamehive.services;

import android.content.Context;
import android.widget.Toast;


public class ToastManager {
    private Toast toast;
    private final Context context;

    public ToastManager(Context context) {
        this.context = context;
    }

    public void displayToast(String text, boolean longDuration) {
        if (toast != null) {
            toast.cancel();
        }
        toast = Toast.makeText(context, text, longDuration ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT);
        toast.show();
    }

    public void displayToast(int resourceId, boolean longDuration) {
        if (toast != null) {
            toast.cancel();
        }
        toast = Toast.makeText(context, resourceId, longDuration ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT);
        toast.show();
    }

    public void displayShortToast(String text) {
        displayToast(text, false);
    }

    public void displayShortToast(int resourceId) {
        displayToast(resourceId, false);
    }

    public void displayLongToast(String text) {
        displayToast(text, true);
    }

    public void displayLongToast(int resourceId) {
        displayToast(resourceId, true);
    }
}
