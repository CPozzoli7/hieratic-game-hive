package it.unimib.hieraticgamehive.services.remote;

import android.util.Log;

import com.api.igdb.apicalypse.APICalypse;
import com.api.igdb.apicalypse.Sort;
import com.api.igdb.exceptions.RequestException;
import com.api.igdb.request.IGDBWrapper;
import com.api.igdb.request.ProtoRequestKt;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executor;

import it.unimib.hieraticgamehive.core.interfaces.OnCompleteCallback;
import it.unimib.hieraticgamehive.services.TokenManager;
import it.unimib.hieraticgamehive.utils.Genres;
import it.unimib.hieraticgamehive.utils.IGDBCredentials;
import it.unimib.hieraticgamehive.utils.IGDBFields;
import it.unimib.hieraticgamehive.utils.Themes;
import proto.Company;
import proto.Game;
import proto.Platform;

/**
 * ITA costruttore di IGDBService
 * ENG constructor of IGDBService
 **/
public class IGDBService {
    private final String TAG = "IGDBService";
    private final IGDBWrapper wrapper = IGDBWrapper.INSTANCE;
    private final TokenManager tokenManager;
    private boolean isInitialized = false;
    private final static int LIMIT_HIGHLIGHT = 20;
    private final static int LIMIT_RESULT = 20;

    /**
     * ITA inserisce il token preso come parametro nella classe e wrappa il client ID delle credenziali
     * ENG takes the token from parameter and put it in the class, and wraps the credential's client ID
     **/
    public IGDBService(TokenManager tokenManager) {
        this.tokenManager = tokenManager;
        Log.d(TAG, "Getting token from TokenManager");
        requestToken(false);
    }

    private synchronized void requestToken(boolean ignoreCache) {
        tokenManager.getToken(ignoreCache, new OnCompleteCallback<String>() {
            @Override
            public void onComplete(String result) {
                setCredentials(result);
            }

            @Override
            public void onError() {
                Log.e(TAG, "Error while getting token from TokenManager"); //TODO Manage error
            }
        });
    }

    private synchronized void setCredentials(String credentials) {
        Log.d(TAG, "setCredentials: setting credentials");
        wrapper.setCredentials(IGDBCredentials.ClientID, credentials);
        isInitialized = true;
        notifyAll();
    }

    /**
     * ITA ottiene una lista di giochi e gestisce gli errori
     * ENG obtains a list of games and handles the errors
     **/
    private List<Game> getGamesList(APICalypse query) {
        if (!isInitialized) {
            synchronized (this) {
                try {
                    while (!isInitialized) {
                        wait();
                    }
                } catch (InterruptedException ignored) {
                }
                Log.d(TAG, "getGamesList: IGDB credential not set yet");
            }
        }
        Log.d(TAG, "getGamesList: Getting game list for query " + query.buildQuery());
        List<Game> gameList = null;
        try {
            gameList = ProtoRequestKt.games(wrapper, query);
        } catch (RequestException e) {
            if (e.getStatusCode() == 401) {
                Log.e(TAG, "getGamesList: ERROR CODE 401 while retrieving game list");
                synchronized (this) {
                    //pendingQueries.add(new PendingQuery(query, callback));
                    if (isInitialized) {
                        isInitialized = false;
                        Log.d(TAG, "getGamesList: Trying to refresh token");
                        requestToken(true);
                    }
                    return getGamesListAfterError(query);
                }
            } else {
                Log.e(TAG, "getGamesList: ERROR while retrieving game list, exception: " + e.getLocalizedMessage());
            }
            return null;
        }
        Log.d(TAG, "getGamesList: returning gameList");
        return gameList;
    }

    private List<Game> getGamesListAfterError(APICalypse query) {
        List<Game> gameList = null;
        synchronized (this) {
            try {
                while (!isInitialized) {
                    wait();
                }
                gameList = ProtoRequestKt.games(wrapper, query);
            } catch (Exception ignored) {
            }
            return gameList;
        }
    }

    /**
     * ITA Ottiene un singolo gioco da una query passata per parametro
     * ENG obtains a single game from a query (parameter)
     **/
    private Game getSingleGame(APICalypse query) {
        List<Game> gameList = getGamesList(query);
        if (gameList != null && gameList.size() > 0) {
            return gameList.get(0);
        } else {
            return null;
        }
    }

    /**
     * ITA query per ottenere un gioco con parametro ID
     * ENG query to obtain a game through ID (parameter)
     **/
    public Game getGame(long id, IGDBFields fields) {
        APICalypse query = new APICalypse()
                .fields(fields.getFields())
                .where("id = " + id);
        return getSingleGame(query);
    }

    public Game getGame(String urlSlugName, IGDBFields fields) {
        APICalypse query = new APICalypse()
                .fields(fields.getFields())
                .where("slug = \"" + urlSlugName + "\"");
        return getSingleGame(query);
    }

    public List<Game> getCompleteMultipleGames(List<Game> gameList, IGDBFields fields) {
        StringBuilder idsBuilder = new StringBuilder();
        idsBuilder.append("(");
        for (int i = 0; i < gameList.size(); i++) {
            if (i != 0) {
                idsBuilder.append(",");
            }
            idsBuilder.append(gameList.get(i).getId());
        }
        idsBuilder.append(")");
        APICalypse query = new APICalypse()
                .fields(fields.getFields())
                .where("id = " + idsBuilder);
        return getGamesList(query);
    }


    public List<Game> getMultipleGames(List<Long> idList, IGDBFields fields) {
        if (idList == null) {
            return Collections.emptyList();
        }
        StringBuilder idsBuilder = new StringBuilder();
        idsBuilder.append("(");
        for (int i = 0; i < idList.size(); i++) {
            if (i != 0) {
                idsBuilder.append(",");
            }
            idsBuilder.append(idList.get(i));
        }
        idsBuilder.append(")");
        APICalypse query = new APICalypse()
                .fields(fields.getFields())
                .where("id = " + idsBuilder)
                .limit(idList.size());
        return getGamesList(query);
    }


    public Platform getPlatform(long id) {
        APICalypse query = new APICalypse()
                .fields("name, summary, platform_logo.image_id, versions.platform_logo.image_id, versions.platform_version_release_dates.date, versions.summary")
                .where("id = " + id);
        try {
            return ProtoRequestKt.platforms(wrapper, query).get(0);
        } catch (RequestException e) {
            return null;
        }
    }

    public Company getCompany(long id) {
        APICalypse query = new APICalypse()
                .fields("name, description, logo.image_id, start_date")
                .where("id = " + id);
        try {
            return ProtoRequestKt.companies(wrapper, query).get(0);
        } catch (RequestException e) {
            return null;
        }
    }

    /**
     * ITA query che permette di ottenere i giochi suggeriti
     * ENG query to obtain the suggested games
     **/
    private final Object firebaseLock = new Object();

    public List<Game> getSuggested(IGDBFields fields) {
        //TODO Cache firebase query
        List<Game> gameList = Collections.emptyList();
        final StringBuilder IdsBuilder = new StringBuilder();
        synchronized (firebaseLock) {
            FirebaseDatabase
                    .getInstance()
                    .getReference()
                    .child("suggested")
                    .get()
                    .addOnSuccessListener(dataSnapshot -> {
                        synchronized (firebaseLock) {
                            IdsBuilder.append(dataSnapshot.getValue());
                            firebaseLock.notify();
                        }
                    })
                    .addOnFailureListener(e -> {
                        synchronized (firebaseLock) {
                            firebaseLock.notify();
                        }
                    });
            try {
                firebaseLock.wait();
                if (!IdsBuilder.toString().isEmpty()) {
                    String ids = (IdsBuilder.substring(IdsBuilder.indexOf("[") + 1, IdsBuilder.lastIndexOf("]")));
                    APICalypse query = new APICalypse()
                            .fields(fields.getFields())
                            .where("id = (" + ids + ")")
                            .sort("id", Sort.ASCENDING)
                            .limit(LIMIT_HIGHLIGHT);
                    gameList = getGamesList(query);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return gameList;
        }
    }

    /**
     * ITA query che permette di ottenere i giochi più anticipati
     * ENG query to obtain the most anticipated games
     **/
    public List<Game> getMostAnticipated(IGDBFields fields) {
        APICalypse query = new APICalypse()
                .fields(fields.getFields())
                .where("hypes != 0 & first_release_date > " + (System.currentTimeMillis() / 1000))
                .sort("hypes", Sort.DESCENDING)
                .limit(LIMIT_HIGHLIGHT);
        return getGamesList(query);
    }

    /**
     * ITA query che permette di ottenere la lista dei giochi più popolari (ancora da sistemare
     * la funzione)
     * ENG query to obtain the most popular games list
     **/
    public List<Game> getMostPopular(IGDBFields fields) {
        APICalypse query = new APICalypse()
                .fields(fields.getFields())
                .where("first_release_date > " + ((System.currentTimeMillis() / 1000) - 3 * 2629743)
                        + " & first_release_date < " + ((System.currentTimeMillis() / 1000) + 3 * 2629743)
                        + " & category = (0, 4, 8, 9) & hypes != 0 & follows != 0 & total_rating_count != 0")
                .sort("total_rating_count", Sort.DESCENDING)
                .limit(LIMIT_HIGHLIGHT);
        return getGamesList(query);
    }

    public List<Game> getLatestReleases(IGDBFields fields) {
        APICalypse query = new APICalypse()
                .fields(fields.getFields())
                .where("category = (0, 4, 8, 9) & total_rating_count != 0 & " +
                        "first_release_date < " + (System.currentTimeMillis() / 1000))
                .sort("first_release_date", Sort.DESCENDING)
                .limit(LIMIT_HIGHLIGHT);
        return getGamesList(query);
    }

    public List<Game> getRetros(IGDBFields fields) {
        APICalypse query = new APICalypse()
                .fields(fields.getFields())
                .where("category = (0, 4, 8, 9) & total_rating_count != 0 & " +
                        "first_release_date < " + (System.currentTimeMillis() / 1000 - 31536000 * 15))
                .sort("total_rating_count", Sort.DESCENDING)
                .limit(LIMIT_HIGHLIGHT);
        return getGamesList(query);
    }

    public List<Game> getBestRated(IGDBFields fields) {
        APICalypse query = new APICalypse()
                .fields(fields.getFields())
                .where("category = (0, 4, 8, 9) & total_rating_count >= 10 & total_rating != 0 & aggregated_rating != 0 & aggregated_rating_count >= 5")
                .sort("aggregated_rating", Sort.DESCENDING)
                .limit(LIMIT_HIGHLIGHT);
        return getGamesList(query);
    }

    public List<Game> getWorstRated(IGDBFields fields) {
        APICalypse query = new APICalypse()
                .fields(fields.getFields())
                .where("category = (0, 4, 8, 9) & total_rating_count >= 10 & total_rating != 0 & aggregated_rating != 0 & aggregated_rating_count >= 5")
                .sort("aggregated_rating", Sort.ASCENDING)
                .limit(LIMIT_HIGHLIGHT);
        return getGamesList(query);
    }

    public List<Game> searchGames(IGDBFields fields, int offset, String search) {
        APICalypse query = new APICalypse()
                .fields(fields.getFields())
                //.where("total_rating_count != 0")
                .search(search)
                .offset(offset * LIMIT_RESULT)
                .limit(LIMIT_RESULT);
        return getGamesList(query);
    }

    /**
     * ITA restituisce una lista di giochi aventi un singolo genere (passato per parametro)
     * in ordine discendente per numero totale di voti
     * ENG Return a list of games having a single genre (passed by parameter) in descending order
     * of total rating count.
     */
    public List<Game> getGamesByGenre(IGDBFields fields, int offset, Genres genre) {
        APICalypse query = new APICalypse()
                .fields(fields.getFields())
                //.where(fields.getDefaultWhere() + "genres.name = \"" + genre.getQueryString() + "\" & total_rating_count != 0")
                .where("genres.name = \"" + genre.getQueryString() + "\" & total_rating_count != 0")
                .sort("total_rating_count", Sort.DESCENDING)
                .offset(offset * LIMIT_RESULT)
                .limit(LIMIT_RESULT);
        return getGamesList(query);
    }

    public List<Game> getGamesByTheme(IGDBFields fields, int offset, Themes theme) {
        APICalypse query = new APICalypse()
                .fields(fields.getFields())
                .where("themes.name = \"" + theme.getQueryString() + "\" & total_rating_count != 0")
                .sort("total_rating_count", Sort.DESCENDING)
                .offset(offset * LIMIT_RESULT)
                .limit(LIMIT_RESULT);
        return getGamesList(query);
    }

    public List<Game> getGamesByCompany(IGDBFields fields, int offset, long company) {
        Log.d(TAG, "getGameByCompany: Started query for: " + company);
//        APICalypse query = new APICalypse()
//                .fields("developed, published")
//                .where("name =  \"" + company + "\"");
//        Company protoCompany = null;
//        try {
//            protoCompany = ProtoRequestKt.companies(wrapper, query).get(0);
//        } catch (RequestException e) {
//            return null;
//        }
//        StringBuilder ids = new StringBuilder();
//        for (int i = 0; i < protoCompany.getDevelopedCount(); i++) {
//            if (i != 0) {
//                ids.append(",");
//            }
//            ids.append(protoCompany.getDeveloped(i).getId());
//        }
//        for (int i = 0; i < protoCompany.getPublishedCount(); i++) {
//            if (i != 0) {
//                ids.append(",");
//            }
//            ids.append(protoCompany.getPublished(i).getId());
//        }
        APICalypse query = new APICalypse()
                .fields(fields.getFields())
                .where("category = 0 & first_release_date != 0 & involved_companies.company = " + company)
                .sort("first_release_date", Sort.DESCENDING)
                .offset(offset * LIMIT_RESULT)
                .limit(LIMIT_RESULT);
        return getGamesList(query);
    }

    public List<Game> getGamesByPlatform(IGDBFields fields, int offset, long platformId) {
        APICalypse query = new APICalypse()
                .fields(fields.getFields())
                .where("platforms = (" + platformId + ") & total_rating_count != 0")
                .sort("total_rating_count", Sort.DESCENDING)
                .offset(offset * LIMIT_RESULT)
                .limit(LIMIT_RESULT);
        return getGamesList(query);
    }

    public List<Game> getGamesByYear(IGDBFields fields, int offset, long unixTimeSeconds) {
        long yearBefore = (unixTimeSeconds - (unixTimeSeconds % 31556926) + 86400);
        long yearAfter = (unixTimeSeconds - (unixTimeSeconds % 31556926) + 31556926);
        String field = (unixTimeSeconds < (System.currentTimeMillis() /1000) ? "total_rating_count" : "hypes");
        APICalypse query = new APICalypse()
                .fields(fields.getFields())
                .where("first_release_date >= " + yearBefore +
                        " & first_release_date <= " + yearAfter +
                        " & " + field + " != 0")
                .sort(field, Sort.DESCENDING)
                .offset(offset * LIMIT_RESULT)
                .limit(LIMIT_RESULT);
        return getGamesList(query);
    }
}
