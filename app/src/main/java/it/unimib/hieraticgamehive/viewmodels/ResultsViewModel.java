package it.unimib.hieraticgamehive.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import it.unimib.hieraticgamehive.core.interfaces.OnCompleteCallback;
import it.unimib.hieraticgamehive.repositories.GameRepository;
import it.unimib.hieraticgamehive.utils.ResultType;
import it.unimib.hieraticgamehive.utils.SearchDetail;
import proto.Game;

public class ResultsViewModel extends GameRepositoryViewModel {
    private final List<Game> gameList = new ArrayList<>();
    private final MutableLiveData<List<Game>> gameListLiveData = new MutableLiveData<>();
    private final MutableLiveData<SearchDetail> searchDetail = new MutableLiveData<>();

    private ResultType resultType;
    private String parameter;

    private boolean isLoading = false;

    public ResultsViewModel(GameRepository gameRepository) {
        super(gameRepository);
    }

    public LiveData<List<Game>> getGameResultLiveData(ResultType resultType, String parameter) {
        this.resultType = resultType;
        this.parameter = parameter;
        if (gameListLiveData.getValue() == null && !isLoading) {
            isLoading = true;
            gameRepository.getGameResults(resultType, parameter, 0, new OnCompleteCallback<List<Game>>() {
                @Override
                public void onComplete(List<Game> result) {
                    gameList.addAll(result);
                    gameListLiveData.postValue(gameList);
                    isLoading = false;
                }

                @Override
                public void onError() {
                    gameListLiveData.postValue(Collections.emptyList());
                    isLoading = false;
                }
            });
        }
        return gameListLiveData;
    }

    private int page = 0;
    private boolean reachedEnd = false;

    public void getMoreGameResults() {
        if (!gameList.isEmpty() && !isLoading && !reachedEnd) {
            isLoading = true;
            gameList.add(null);
            gameListLiveData.postValue(gameList);
            gameRepository.getGameResults(resultType, parameter, ++page, new OnCompleteCallback<List<Game>>() {
                @Override
                public void onComplete(List<Game> result) {
                    if (gameList.size() > 0 && gameList.get(gameList.size() - 1) == null) {
                        gameList.remove(gameList.size() - 1);
                    }
                    if (result.size() == 0) {
                        reachedEnd = true;
                    } else {
                        gameList.addAll(result);
                        gameListLiveData.postValue(gameList);
                    }
                    isLoading = false;
                }

                @Override
                public void onError() {
                }
            });
        }
    }


    public LiveData<SearchDetail> getSearchDetailLiveData(ResultType resultType, String parameter) {
        if (searchDetail.getValue() == null) {
            gameRepository.getSearchDetail(resultType, parameter, new OnCompleteCallback<SearchDetail>() {
                @Override
                public void onComplete(SearchDetail result) {
                    searchDetail.postValue(result);
                }

                @Override
                public void onError() {
                }
            });
        }
        return searchDetail;
    }
}