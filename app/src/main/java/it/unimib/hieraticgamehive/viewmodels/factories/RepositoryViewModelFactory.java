package it.unimib.hieraticgamehive.viewmodels.factories;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import org.jetbrains.annotations.NotNull;

import it.unimib.hieraticgamehive.repositories.GameRepository;
import it.unimib.hieraticgamehive.repositories.UserRepository;
import it.unimib.hieraticgamehive.viewmodels.GameDetailViewModel;
import it.unimib.hieraticgamehive.viewmodels.HomeViewModelGame;
import it.unimib.hieraticgamehive.viewmodels.GameRepositoryViewModel;
import it.unimib.hieraticgamehive.viewmodels.ProfileViewModel;
import it.unimib.hieraticgamehive.viewmodels.ResultsViewModel;

public class RepositoryViewModelFactory implements ViewModelProvider.Factory {
    private final GameRepository gameRepository;
    private final UserRepository userRepository;

    public RepositoryViewModelFactory(GameRepository gameRepository, UserRepository userRepository) {
        this.gameRepository = gameRepository;
        this.userRepository = userRepository;
    }

    @NotNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        GameRepositoryViewModel viewModel;
        if (HomeViewModelGame.class.equals(modelClass)) {
            viewModel = new HomeViewModelGame(gameRepository);
        } else if (ResultsViewModel.class.equals(modelClass)) {
            viewModel = new ResultsViewModel(gameRepository);
        } else if (GameDetailViewModel.class.equals(modelClass)) {
            viewModel = new GameDetailViewModel(gameRepository, userRepository);
        } else if (ProfileViewModel.class.equals(modelClass)) {
            viewModel = new ProfileViewModel(gameRepository, userRepository);
        } else {
            viewModel = new HomeViewModelGame(gameRepository);
        }
        return (T) viewModel;
    }
}
