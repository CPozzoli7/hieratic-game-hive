package it.unimib.hieraticgamehive.viewmodels;

import androidx.lifecycle.ViewModel;

import it.unimib.hieraticgamehive.repositories.GameRepository;
import proto.Game;

public abstract class GameRepositoryViewModel extends ViewModel {
    protected final GameRepository gameRepository;

    public GameRepositoryViewModel(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    public void setSelectedGame(Game game) {
        gameRepository.setSelectedGame(game);
    }
}
