package it.unimib.hieraticgamehive.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.Map;
import java.util.List;

import it.unimib.hieraticgamehive.core.interfaces.OnCompleteCallback;
import it.unimib.hieraticgamehive.repositories.GameRepository;
import proto.Game;

public class HomeViewModelGame extends GameRepositoryViewModel {
    private final MutableLiveData<Map<Integer, List<Game>>> highlights = new MutableLiveData<>();

    public HomeViewModelGame(GameRepository gameRepository) {
        super(gameRepository);
    }


    public LiveData<Map<Integer, List<Game>>> getHighlights() {
        if(highlights.getValue() == null) {
            gameRepository.getHighlights(new OnCompleteCallback<Map<Integer, List<Game>>>() {
                @Override
                public void onComplete(Map<Integer, List<Game>> result) {
                    highlights.postValue(result);
                }

                @Override
                public void onError() {
                }
            });
        }
        return highlights;
    }
}