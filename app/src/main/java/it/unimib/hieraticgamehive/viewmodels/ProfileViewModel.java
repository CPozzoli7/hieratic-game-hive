package it.unimib.hieraticgamehive.viewmodels;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.firebase.ui.auth.data.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import it.unimib.hieraticgamehive.core.interfaces.OnCompleteCallback;
import it.unimib.hieraticgamehive.repositories.GameRepository;
import it.unimib.hieraticgamehive.repositories.UserRepository;
import it.unimib.hieraticgamehive.services.remote.IGDBService;
import proto.Game;

public class ProfileViewModel extends GameRepositoryViewModel {
    private final UserRepository userRepository;
    private boolean isCollectionSelected = true;

    public boolean isCollectionSelected() {
        return isCollectionSelected;
    }

    public void setCollectionSelected(boolean collectionSelected) {
        isCollectionSelected = collectionSelected;
    }

    private final MutableLiveData<FirebaseUser> user = new MutableLiveData<>();
    private final MutableLiveData<List<Game>> collection = new MutableLiveData<>();
    private final MutableLiveData<List<Game>> wishlist = new MutableLiveData<>();


    public ProfileViewModel(GameRepository gameRepository, UserRepository userRepository) {
        super(gameRepository);
        this.userRepository = userRepository;
        userRepository.addUserListener(firebaseAuth -> user.postValue(firebaseAuth.getCurrentUser()));
    }

    public LiveData<FirebaseUser> getUserLiveData() {
        return user;
    }

    public LiveData<List<Game>> getCollectionLiveData() {
        userRepository.getCollection(new OnCompleteCallback<List<Long>>() {
            @Override
            public void onComplete(List<Long> result) {
                getGames(result, collection);
            }

            @Override
            public void onError() {
            }
        });
        return collection;
    }

    public LiveData<List<Game>> getWishlistLiveData() {
        userRepository.getWishlist(new OnCompleteCallback<List<Long>>() {
            @Override
            public void onComplete(List<Long> result) {
                getGames(result, wishlist);
            }

            @Override
            public void onError() {
            }
        });
        return wishlist;
    }

    private void getGames(List<Long> ids, MutableLiveData<List<Game>> livedata) {
        gameRepository.getMultipleGames(ids, new OnCompleteCallback<List<Game>>() {
            @Override
            public void onComplete(List<Game> result) {
                livedata.postValue(result);
            }

            @Override
            public void onError() {
            }
        });

    }
}