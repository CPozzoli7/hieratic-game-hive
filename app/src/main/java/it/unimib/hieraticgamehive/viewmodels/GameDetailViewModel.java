package it.unimib.hieraticgamehive.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.Collections;
import java.util.List;

import it.unimib.hieraticgamehive.core.interfaces.OnCompleteCallback;
import it.unimib.hieraticgamehive.repositories.GameRepository;
import it.unimib.hieraticgamehive.repositories.UserRepository;
import proto.Game;

public class GameDetailViewModel extends GameRepositoryViewModel {
    private final MutableLiveData<Game> game = new MutableLiveData<>();
    private final MutableLiveData<List<Game>> similarGames = new MutableLiveData<>();
    private final UserRepository userRepository;
    private final MutableLiveData<Boolean> isLoading = new MutableLiveData<>(false);

    public GameDetailViewModel(GameRepository gameRepository, UserRepository userRepository) {
        super(gameRepository);
        this.userRepository = userRepository;
    }

    public LiveData<Boolean> getIsLoadingLiveData() {
        return isLoading;
    }

    private OnCompleteCallback<Game> getOnCompleteCallbackForGame(MutableLiveData<Game> liveData) {
        return new OnCompleteCallback<Game>() {
            @Override
            public void onComplete(Game result) {
                liveData.postValue(result);
                isLoading.postValue(false);
            }

            @Override
            public void onError() {
                liveData.postValue(null);
                isLoading.postValue(false);
            }
        };
    }

    public LiveData<Game> getGame(long gameId) {
        isLoading.postValue(true);
        Game partialGame = gameRepository.getGame(gameId, getOnCompleteCallbackForGame(game));
        game.postValue(partialGame);
        return game;
    }

    public LiveData<Game> getGame(String gamePath) {
        isLoading.postValue(true);
        gameRepository.getGame(gamePath, getOnCompleteCallbackForGame(game));
        return game;
    }

    public LiveData<List<Game>> getSimilarGames(List<Game> similarGamesList) {
        gameRepository.getSimilarGames(similarGamesList, new OnCompleteCallback<List<Game>>() {
            @Override
            public void onComplete(List<Game> result) {
                similarGames.postValue(result);
            }

            @Override
            public void onError() {
                similarGames.postValue(Collections.emptyList());
            }
        });
        return similarGames;
    }

    public UserRepository getUserRepository() {
        return userRepository;
    }

    public boolean addGameToCollection(long id) {
        return userRepository.addToCollection(id);
    }

    public boolean addGameToWishlist(long id) {
        return userRepository.addToWishlist(id);
    }

    public boolean isGameInCollection(long id) {
        return userRepository.isInCollection(id);
    }

    public boolean isGameInWishlist(long id) {
        return userRepository.isInWishlist(id);
    }

    public boolean removeGameFromCollection(long id) {
        return userRepository.removeFromCollection(id);
    }

    public boolean removeGameFromWishlist(long id) {
        return userRepository.removeFromWishlist(id);
    }
}
