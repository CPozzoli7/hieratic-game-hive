package it.unimib.hieraticgamehive.viewmodels.interfaces;

import androidx.lifecycle.LiveData;

import java.util.List;

import it.unimib.hieraticgamehive.utils.Highlights;
import proto.Game;

public interface GameListProvider {
    LiveData<List<Game>> getGameListLiveData(Highlights highlight);
}
