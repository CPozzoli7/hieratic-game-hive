package it.unimib.hieraticgamehive.utils;

import android.content.res.Resources;

import androidx.annotation.NonNull;

import java.util.HashMap;
import java.util.Map;

import it.unimib.hieraticgamehive.R;
import proto.Genre;


public enum Genres implements DetailEnum{
    ADVENTURE("Adventure") {
        public int getTitleId() {
            return R.string.adventure;
        }
    },
    ARCADE("Arcade") {
        public int getTitleId() {
            return R.string.arcade;
        }
    },
    CARD_AND_BOARD_GAME("Card & Board Game") {
        public int getTitleId() {
            return R.string.card_and_board_game;
        }
    },
    FIGHTING("Fighting") {
        @Override
        public int getTitleId() {
            return R.string.fighting;
        }
    },
    HACK_AND_SLASH_BEAT_EM_UP("Hack and slash/Beat 'em up") {
        @Override
        public int getTitleId() {
            return R.string.hack_and_slash_beat_em_up;
        }
    },
    INDIE("Indie") {
        @Override
        public int getTitleId() {
            return R.string.indie;
        }
    },
    MOBA("MOBA") {
        @Override
        public int getTitleId() {
            return R.string.moba;
        }
    },
    MUSIC("Music") {
        @Override
        public int getTitleId() {
            return R.string.music;
        }
    },
    PINBALL("Pinball") {
        @Override
        public int getTitleId() {
            return R.string.pinball;
        }
    },
    PLATFORM("Platform") {
        public int getTitleId() {
            return R.string.platform;
        }
    },
    POINT_AND_CLICK("Point-and-click") {
        @Override
        public int getTitleId() {
            return R.string.point_and_click;
        }
    },
    PUZZLE("Puzzle") {
        public int getTitleId() {
            return R.string.puzzle;
        }
    },
    QUIZ_TRIVIA("Quiz/Trivia") {
        public int getTitleId() {
            return R.string.quiz_trivia;
        }
    },
    RACING("Racing") {
        public int getTitleId() {
            return R.string.racing;
        }
    },
    REAL_TIME_STRATEGY("Real Time Strategy (RTS)") {
        public int getTitleId() {
            return R.string.real_time_strategy;
        }
    },
    ROLE_PLAYING("Role-playing (RPG)") {
        public int getTitleId() {
            return R.string.role_playing;
        }
    },
    SHOOTER("Shooter") {
        @Override
        public int getTitleId() {
            return R.string.shooter;
        }
    },
    SIMULATOR("Simulator") {
        @Override
        public int getTitleId() {
            return R.string.simulator;
        }
    },
    SPORT("Sport") {
        @Override
        public int getTitleId() {
            return R.string.sport;
        }
    },
    STRATEGY("Strategy") {
        @Override
        public int getTitleId() {
            return R.string.strategy;
        }
    },
    TACTICAL("Tactical") {
        @Override
        public int getTitleId() {
            return R.string.tactical;
        }
    },
    TURN_BASED_STRATEGY("Turn-based strategy (TBS)") {
        @Override
        public int getTitleId() {
            return R.string.turn_based_strategy;
        }
    },
    VISUAL_NOVEL("Visual Novel") {
        @Override
        public int getTitleId() {
            return R.string.visual_novel;
        }
    },
    ;

    private final String genre;

    private static final Map<String, Genres> map = new HashMap<>();

    static {
        for (Genres genre : values()) {
            map.put(genre.genre, genre);
        }
    }

    Genres(String genre) {
        this.genre = genre;
    }

    public String getQueryString() {
        return genre;
    }

    public static Genres valueOf(@NonNull Genre protoGenre) {
        return map.getOrDefault(protoGenre.getName(), null);
    }

    public abstract int getTitleId();
}
