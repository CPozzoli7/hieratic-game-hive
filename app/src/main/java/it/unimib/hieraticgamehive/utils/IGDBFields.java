package it.unimib.hieraticgamehive.utils;

//result type for the visualisation of the query
public enum IGDBFields { //TODO Fix queries
    ALL("*"),
    NAME("name"),
    COVER("name," +
            "cover.image_id," +
            "summary," +
            "first_release_date," +
            "involved_companies.developer," +
            "involved_companies.publisher," +
            "involved_companies.company.name"
    ) {
        @Override
        public String getDefaultWhere() {
            return "";//"involved_companies.developer = true &";
        }
    },
    COVER_DETAIL(
            "name," +
                    "platforms.name," +
                    "cover.image_id," +
                    "summary," +
                    "first_release_date," +
                    "involved_companies.developer," +
                    "involved_companies.publisher," +
                    "involved_companies.company.name," +
                    "rating," +
                    "aggregated_rating"
    ) {
        @Override
        public String getDefaultWhere() {
            return "";//"involved_companies.developer = true &";
        }
    },
    DETAIL(
            "*," +
                    "cover.image_id," +
                    "platforms.name," +
                    "genres.name," +
                    "themes.name," +
                    "involved_companies.developer," +
                    "involved_companies.publisher," +
                    "involved_companies.company.name," +
                    "similar_games.cover.image_id," +
                    "screenshots.image_id"
    ) {
        @Override
        public String getDefaultWhere() {
            return "";//"involved_companies.developer = true &";
        }
    };

    private final String fields;

    IGDBFields(String value) {
        this.fields = value;
    }

    public String getFields() {
        return fields;
    }

    public String getDefaultWhere() {
        return "";
    }
}
