package it.unimib.hieraticgamehive.utils;

import it.unimib.hieraticgamehive.R;

public enum Highlights {
    MOST_POPULAR {
        @Override
        public int getTitleId() {
            return R.string.most_popular;
        }
    },
    MOST_ANTICIPATED {
        @Override
        public int getTitleId() {
            return R.string.most_anticipated;
        }
    },
    SUGGESTED {
        @Override
        public int getTitleId() {
            return R.string.suggested;
        }
    },
    LATEST_RELEASES {
        @Override
        public int getTitleId() {
            return R.string.latest_release;
        }
    },
    RETROS {
        @Override
        public int getTitleId() {
            return R.string.retro;
        }
    },
    BEST_RATED {
        @Override
        public int getTitleId() {
            return R.string.best_rated;
        }
    },
    WORST_RATED {
        @Override
        public int getTitleId() {
            return R.string.worst_rated;
        }
    };


    public abstract int getTitleId();
}
