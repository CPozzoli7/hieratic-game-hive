package it.unimib.hieraticgamehive.utils;

public interface DetailEnum {
    int getTitleId();
    String getQueryString();
    String name();
}
