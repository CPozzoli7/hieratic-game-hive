package it.unimib.hieraticgamehive.utils;

public class SearchDetail {
    private final String name;
    private final String imageId;
    private final String releaseDate;
    private final String summary;

    public SearchDetail(String name, String imageId, String releaseDate, String summary) {
        this.name = name;
        this.imageId = imageId;
        this.releaseDate = releaseDate;
        this.summary = summary;
    }

    public String getName() {
        return name;
    }

    public String getImageId() {
        return imageId;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public String getSummary() {
        return summary;
    }
}
