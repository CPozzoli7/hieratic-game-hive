package it.unimib.hieraticgamehive.utils;

public enum ResultType {
    SEARCH,
    COLLECTION,
    WISHLIST,
    GENRE,
    THEME,
    PLATFORM,
    COMPANY,
    YEAR,
    ;
}
