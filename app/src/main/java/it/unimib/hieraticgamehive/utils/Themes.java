package it.unimib.hieraticgamehive.utils;

import androidx.annotation.NonNull;

import java.util.HashMap;
import java.util.Map;

import it.unimib.hieraticgamehive.R;
import proto.Theme;;

public enum Themes implements DetailEnum{
    FANTASY("Fantasy") {
        @Override
        public int getTitleId() {
            return R.string.theme_fantasy;
        }
    },
    THRILLER("Thriller") {
        @Override
        public int getTitleId() {
            return R.string.theme_thriller;
        }
    },
    SCIENCE_FICTION("Science fiction") {
        @Override
        public int getTitleId() {
            return R.string.theme_science_fiction;
        }
    },
    ACTION("Action") {
        @Override
        public int getTitleId() {
            return R.string.theme_action;
        }
    },
    HORROR("Horror") {
        @Override
        public int getTitleId() {
            return R.string.theme_horror;
        }
    },
    SURVIVAL("Survival") {
        @Override
        public int getTitleId() {
            return R.string.theme_survival;
        }
    },
    HISTORICAL("Historical") {
        @Override
        public int getTitleId() {
            return R.string.theme_historical;
        }
    },
    STEALTH("Stealth") {
        @Override
        public int getTitleId() {
            return R.string.theme_stealth;
        }
    },
    BUSINESS("Business") {
        @Override
        public int getTitleId() {
            return R.string.theme_business;
        }
    },
    COMEDY("Comedy") {
        @Override
        public int getTitleId() {
            return R.string.theme_comedy;
        }
    },
    DRAMA("Drama") {
        @Override
        public int getTitleId() {
            return R.string.theme_drama;
        }
    },
    NON_FICTION("Non-fiction") {
        @Override
        public int getTitleId() {
            return R.string.theme_non_fiction;
        }
    },
    EDUCATIONAL("Educational") {
        @Override
        public int getTitleId() {
            return R.string.theme_educational;
        }
    },
    SANDBOX("Sandbox") {
        @Override
        public int getTitleId() {
            return R.string.theme_sandbox;
        }
    },
    KIDS("Kids") {
        @Override
        public int getTitleId() {
            return R.string.theme_kids;
        }
    },
    OPEN_WORLD("Open world") {
        @Override
        public int getTitleId() {
            return R.string.theme_open_world;
        }
    },
    WARFARE("Warfare") {
        @Override
        public int getTitleId() {
            return R.string.theme_warfare;
        }
    },
    XXXX("4X (explore, expand, exploit, and exterminate)") {
        @Override
        public int getTitleId() {
            return R.string.theme_4x;
        }
    },
    EROTIC("Erotic") {
        @Override
        public int getTitleId() {
            return R.string.theme_erotic;
        }
    },
    MYSTERY("Mystery") {
        @Override
        public int getTitleId() {
            return R.string.theme_mystey;
        }
    },
    PARTY("Party") {
        @Override
        public int getTitleId() {
            return R.string.theme_party;
        }
    },
    ROMANCE("Romance") {
        @Override
        public int getTitleId() {
            return R.string.theme_romance;
        }
    },
    ;

    private final String theme;
    private static final Map<String, Themes> map = new HashMap<>();

    static {
        for (Themes theme : values()) {
            map.put(theme.theme, theme);
        }
    }

    Themes(String theme) {
        this.theme = theme;
    }

    public String getQueryString() {
        return theme;
    }

    public static Themes valueOf(@NonNull Theme protoTheme) {
        return map.getOrDefault(protoTheme.getName(), null);
    }

    public abstract int getTitleId();
}
