package it.unimib.hieraticgamehive.ui.adapters;

import android.content.Context;
import android.util.Pair;

import androidx.annotation.NonNull;

import it.unimib.hieraticgamehive.utils.ResultType;

public class GameDetailPairListAdapter extends GameDetailListAdapter<Pair<Long, String>> {

    public GameDetailPairListAdapter(Context context, @NonNull AdapterType adapterType, @NonNull ResultType resultType) {
        super(context, adapterType, resultType);
    }

    @Override
    String getDisplayedValue(int position) {
        return getCurrentList().get(position).second;
    }

    @Override
    String getResultsParameterValue(int position) {
        return String.valueOf(getCurrentList().get(position).first);
    }
}
