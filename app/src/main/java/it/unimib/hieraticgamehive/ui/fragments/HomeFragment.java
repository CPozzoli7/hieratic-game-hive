package it.unimib.hieraticgamehive.ui.fragments;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;
import androidx.vectordrawable.graphics.drawable.Animatable2Compat;
import androidx.vectordrawable.graphics.drawable.AnimatedVectorDrawableCompat;

import it.unimib.hieraticgamehive.R;
import it.unimib.hieraticgamehive.ui.adapters.HighlightsRecyclerAdapter;
import it.unimib.hieraticgamehive.core.MainActivity;
import it.unimib.hieraticgamehive.core.interfaces.ItemReselectedFragment;
import it.unimib.hieraticgamehive.viewmodels.HomeViewModelGame;
import it.unimib.hieraticgamehive.viewmodels.factories.RepositoryViewModelFactory;

public class HomeFragment extends GameFragment<HomeViewModelGame> implements ItemReselectedFragment {
    private RecyclerView highlightsRecyclerView;
    private ImageView honeycombLoading;
    private AnimatedVectorDrawableCompat animation;

    private boolean animationShown = false;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        RepositoryViewModelFactory viewModelFactory = ((MainActivity) requireActivity()).getAppContainer().repositoryViewModelFactory;
        HomeViewModelGame viewModel =
                new ViewModelProvider(requireActivity(), viewModelFactory).get(HomeViewModelGame.class);
        setViewModel(viewModel);
        View root = inflater.inflate(R.layout.fragment_home, container, false); //TODO Fix OnCreateView on change section
        highlightsRecyclerView = root.findViewById(R.id.highlights_recycler_view);
        honeycombLoading = root.findViewById(R.id.honeycomb_loading_home);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        honeycombLoading.setVisibility(View.GONE);
        animation = AnimatedVectorDrawableCompat.create(requireContext(), R.drawable.honeycomb_loading_anim);
        honeycombLoading.setImageDrawable(animation);

        if (!animationShown) {
            highlightsRecyclerView.setAlpha(0);
            setUpAnimation();
        }

        if (highlightsRecyclerView.getAdapter() == null) { //TODO Repeat request considering re-fetch empty failed highlights
            viewModel.getHighlights().observe(getViewLifecycleOwner(), highlights -> {
                if (highlights.size() != 0) {
                    HighlightsRecyclerAdapter highlightsRecyclerAdapter =
                            new HighlightsRecyclerAdapter(
                                    requireContext(),
                                    highlights,
                                    getOnGameClickedListener()
                            );
                    highlightsRecyclerView.setAdapter(highlightsRecyclerAdapter);
                    if (!animationShown) {
                        viewVisible();
                    }
                }
            });
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!animationShown) {
            honeycombLoading.postDelayed(() -> animation.start(), 200);
        }
    }

    private void setUpAnimation() {
        honeycombLoading.setVisibility(View.VISIBLE);
        honeycombLoading.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (honeycombLoading.getWidth() != 0) {
                    honeycombLoading.setPivotX((honeycombLoading.getWidth() * 42.5f) / 100);
                    honeycombLoading.setPivotY((honeycombLoading.getWidth() * 50f) / 100);
                    honeycombLoading.setRotation(14);
                    honeycombLoading.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            }
        });
        animation.registerAnimationCallback(new Animatable2Compat.AnimationCallback() {
            @Override
            public void onAnimationEnd(Drawable drawable) {
                    honeycombLoading.setRotation(honeycombLoading.getRotation() % 360 + 120f);
                    animation.start();
            }
        });
    }

    private void viewVisible() {
        animation.clearAnimationCallbacks();
        animation.registerAnimationCallback(new Animatable2Compat.AnimationCallback() {
            @Override
            public void onAnimationEnd(Drawable drawable) {
                animation.stop();
                honeycombLoading.animate().scaleX(0).scaleY(0).setInterpolator(new AccelerateInterpolator()).setDuration(180).start();
                honeycombLoading.animate().withEndAction(() -> honeycombLoading.setVisibility(View.GONE));
                highlightsRecyclerView.animate().alpha(1).setStartDelay(200).start();
                animationShown = true;
                animation.clearAnimationCallbacks();
            }
        });
    }

    @Override
    public void onItemReselected() {
        highlightsRecyclerView.smoothScrollToPosition(0);
    }
}