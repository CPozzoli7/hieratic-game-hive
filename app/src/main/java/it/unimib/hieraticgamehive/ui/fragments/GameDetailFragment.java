package it.unimib.hieraticgamehive.ui.fragments;

import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavOptions;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.api.igdb.utils.ImageBuilderKt;
import com.api.igdb.utils.ImageSize;
import com.api.igdb.utils.ImageType;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;

import com.google.android.flexbox.AlignItems;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import it.unimib.hieraticgamehive.R;
import it.unimib.hieraticgamehive.core.MainActivity;
import it.unimib.hieraticgamehive.decorations.CoverImageItemDecoration;
import it.unimib.hieraticgamehive.decorations.GameDetailItemDecoration;
import it.unimib.hieraticgamehive.services.ToastManager;
import it.unimib.hieraticgamehive.ui.adapters.CoverRecyclerAdapter;
import it.unimib.hieraticgamehive.ui.adapters.GameDetailEnumListAdapter;
import it.unimib.hieraticgamehive.ui.adapters.GameDetailListAdapter.AdapterType;
import it.unimib.hieraticgamehive.ui.adapters.GameDetailPairListAdapter;
import it.unimib.hieraticgamehive.ui.adapters.ScreenshotRecyclerAdapter;
import it.unimib.hieraticgamehive.utils.DetailEnum;
import it.unimib.hieraticgamehive.utils.Genres;
import it.unimib.hieraticgamehive.utils.ResultType;
import it.unimib.hieraticgamehive.utils.Themes;
import it.unimib.hieraticgamehive.viewmodels.GameDetailViewModel;
import it.unimib.hieraticgamehive.viewmodels.factories.RepositoryViewModelFactory;
import proto.Game;
import proto.Genre;
import proto.InvolvedCompany;
import proto.Platform;
import proto.Screenshot;
import proto.Theme;

import static java.lang.Math.floor;

public class GameDetailFragment extends GameFragment<GameDetailViewModel> {
    private static final String TAG = "GameDetailFragment";
    public static final String BOOLEAN_IS_INTENT_KEY = "IS_INTENT";
    public static final String LONG_GAME_ID_KEY = "GAME_ID";
    public static final String STRING_GAME_URL_PATH_KEY = "GAME_URL_PATH";

    private boolean openFromIntent = false;
    private long gameId = 0;
    private String gameUrlPath = "";

    private ScrollView detailScrollView;
    private TextView gameNameTextView;
    private ImageView gameCoverImageView;
    private TextView gameUsersRatingText;
    private ProgressBar userRatingProgressBar;
    private TextView gameCriticsRatingText;
    private ProgressBar criticRatingProgressBar;
    private GameDetailPairListAdapter developerAdapter;
    private GameDetailPairListAdapter publisherAdapter;
    private GameDetailPairListAdapter platformAdapter;
    private GameDetailEnumListAdapter genresAdapter;
    private GameDetailEnumListAdapter themesAdapter;

    private RecyclerView gameDeveloperRecyclerView;
    private RecyclerView gamePublisherRecyclerView;
    private RecyclerView gamePlatformRecyclerView;
    private RecyclerView gameGenreRecyclerView;
    private RecyclerView gameThemeRecyclerView;
    private TextView gameReleaseView;
    private TextView gameDescriptionView;

    private View developerContainer;
    private View publisherContainer;
    private View releaseContainer;
    private View descriptionContainer;
    private View platformContainer;
    private View genreContainer;
    private View themeContainer;
    private View similarGamesContainer;
    private View screenshotContainer;

    private RecyclerView similarGamesRecycler;
    private CoverRecyclerAdapter coverAdapter;
    private RecyclerView screenshotRecycler;
    private ScreenshotRecyclerAdapter screenshotAdapter;

    private LinearLayout buttonContainer;
    private LinearLayout loginContainer;
    private Button collectionButton;
    private Button wishlistButton;
    private Button loginButton;

    private ProgressBar loadingProgressBar;

    private TextView userText;
    private TextView criticText;

    private ImageView upButton;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_game_detail, container, false);
        if (getArguments() != null) {
            openFromIntent = getArguments().getBoolean(BOOLEAN_IS_INTENT_KEY, false);
            gameId = getArguments().getLong(LONG_GAME_ID_KEY, 0);
            gameUrlPath = getArguments().getString(STRING_GAME_URL_PATH_KEY, "");
        }
        RepositoryViewModelFactory viewModelFactory = ((MainActivity) requireActivity()).getAppContainer().repositoryViewModelFactory;
        GameDetailViewModel viewModel = new ViewModelProvider(this, viewModelFactory).get(GameDetailViewModel.class);
        setViewModel(viewModel);

        upButton = root.findViewById(R.id.game_detail_up_button);

        detailScrollView = root.findViewById(R.id.detail_scroll_view);
        gameNameTextView = root.findViewById(R.id.game_name);
        gameCoverImageView = root.findViewById(R.id.game_cover);
        gameUsersRatingText = root.findViewById(R.id.user_rating_text);
        userRatingProgressBar = root.findViewById(R.id.users_rating);
        gameCriticsRatingText = root.findViewById(R.id.critic_rating_text);
        criticRatingProgressBar = root.findViewById(R.id.critics_rating);
        gameDeveloperRecyclerView = root.findViewById(R.id.game_developer_list);
        developerContainer = root.findViewById(R.id.developers_container_linear_layout);
        gamePublisherRecyclerView = root.findViewById(R.id.game_publisher_list);
        publisherContainer = root.findViewById(R.id.publishers_container_linear_layout);
        gameReleaseView = root.findViewById(R.id.game_release_date);
        releaseContainer = root.findViewById(R.id.release_container_linear_layout);
        gameDescriptionView = root.findViewById(R.id.game_description_text);
        platformContainer = root.findViewById(R.id.game_platforms);

        userText = root.findViewById(R.id.user_text);
        criticText = root.findViewById(R.id.critics_text);

        gamePlatformRecyclerView = root.findViewById(R.id.game_platform_list);
        gameGenreRecyclerView = root.findViewById(R.id.game_genre_list);
        gameThemeRecyclerView = root.findViewById(R.id.game_theme_list);
        similarGamesRecycler = root.findViewById(R.id.game_similar_games_list);

        descriptionContainer = root.findViewById(R.id.description_container);
        genreContainer = root.findViewById(R.id.game_genre);
        themeContainer = root.findViewById(R.id.game_themes);
        similarGamesContainer = root.findViewById(R.id.similar_game_container);
        screenshotContainer = root.findViewById(R.id.screenshot_container);

        screenshotRecycler = root.findViewById(R.id.game_screenshots_list);

        loadingProgressBar = root.findViewById(R.id.detail_loading_progressBar);

        buttonContainer = root.findViewById(R.id.buttons_container);
        loginContainer = root.findViewById(R.id.login_container);
        collectionButton = root.findViewById(R.id.add_collection_button);
        collectionButton.setText(viewModel.isGameInCollection(gameId) ? getString(R.string.profile_remove_collection) : getString(R.string.profile_add_collection));
        wishlistButton = root.findViewById(R.id.add_wishlist_button);
        wishlistButton.setText(viewModel.isGameInWishlist(gameId) ? getString(R.string.profile_remove_wishlist) : getString(R.string.profile_add_wishlist));
        loginButton = root.findViewById(R.id.login_button);

        //setButtonsDimensions(collectionButton, wishlistButton);

        userRatingProgressBar.setProgress(0);
        criticRatingProgressBar.setProgress(0);

        buttonContainer.setVisibility(View.GONE);
        loginContainer.setVisibility(View.GONE);

        developerContainer.setVisibility(View.GONE);
        publisherContainer.setVisibility(View.GONE);
        releaseContainer.setVisibility(View.GONE);
        descriptionContainer.setVisibility(View.GONE);
        platformContainer.setVisibility(View.GONE);
        genreContainer.setVisibility(View.GONE);
        themeContainer.setVisibility(View.GONE);
        similarGamesContainer.setVisibility(View.GONE);
        screenshotContainer.setVisibility(View.GONE);


        gameDeveloperRecyclerView.setVisibility(View.GONE);
        View developerLabel = root.findViewById(R.id.developer_label);
        addRecyclerDecoration(gameDeveloperRecyclerView, developerLabel);
        gameDeveloperRecyclerView.setLayoutManager(getFlexLayoutManager());
        developerAdapter = new GameDetailPairListAdapter(requireContext(), AdapterType.TEXT, ResultType.COMPANY);
        gameDeveloperRecyclerView.setAdapter(developerAdapter);

        gamePublisherRecyclerView.setVisibility(View.GONE);
        View publisherLabel = root.findViewById(R.id.publisher_label);
        addRecyclerDecoration(gamePublisherRecyclerView, publisherLabel);
        gamePublisherRecyclerView.setLayoutManager(getFlexLayoutManager());
        publisherAdapter = new GameDetailPairListAdapter(requireContext(), AdapterType.TEXT, ResultType.COMPANY);
        gamePublisherRecyclerView.setAdapter(publisherAdapter);

        gamePlatformRecyclerView.setVisibility(View.GONE);
        View platformLabel = root.findViewById(R.id.platforms_label);
        addRecyclerDecoration(gamePlatformRecyclerView, platformLabel);
        gamePlatformRecyclerView.setLayoutManager(getFlexLayoutManager());
        platformAdapter = new GameDetailPairListAdapter(requireContext(), AdapterType.TEXT, ResultType.PLATFORM);
        gamePlatformRecyclerView.setAdapter(platformAdapter);


        gameGenreRecyclerView.setLayoutManager(getFlexLayoutManager());
        genresAdapter = new GameDetailEnumListAdapter(requireContext(), AdapterType.BUTTON, ResultType.GENRE);
        gameGenreRecyclerView.setAdapter(genresAdapter);

        gameThemeRecyclerView.setLayoutManager(getFlexLayoutManager());
        themesAdapter = new GameDetailEnumListAdapter(requireContext(), AdapterType.BUTTON, ResultType.THEME);
        gameThemeRecyclerView.setAdapter(themesAdapter);

        coverAdapter = new CoverRecyclerAdapter(requireContext());
        similarGamesRecycler.setAdapter(coverAdapter);

        screenshotAdapter = new ScreenshotRecyclerAdapter(requireContext());
        screenshotRecycler.setAdapter(screenshotAdapter);


        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d(TAG, "onViewCreated: started");
        upButton.setOnClickListener(v -> {
            if (getActivity() != null) {
                getActivity().onBackPressed();
            }
        });

        Observer<Game> observer = game -> {
            if (viewModel.getUserRepository().isLogged()) {
                loginContainer.setVisibility(View.GONE);
                buttonContainer.setVisibility(View.VISIBLE);
            } else {
                buttonContainer.setVisibility(View.GONE);
                loginContainer.setVisibility(View.VISIBLE);
            }
            collectionButton.setOnClickListener(v -> {
                boolean isInCollection = viewModel.isGameInCollection(gameId);
                boolean added;
                if (isInCollection) {
                    added = !viewModel.removeGameFromCollection(gameId);
                } else {
                    added = viewModel.addGameToCollection(gameId);
                }
                collectionButton.setText(added ? getString(R.string.profile_remove_collection) : getString(R.string.profile_add_collection));
            });
            wishlistButton.setOnClickListener(v -> {
                boolean isInWishlist = viewModel.isGameInWishlist(gameId);
                boolean added;
                if (isInWishlist) {
                    added = !viewModel.removeGameFromWishlist(gameId);
                } else {
                    added = viewModel.addGameToWishlist(gameId);
                }
                wishlistButton.setText(added ? getString(R.string.profile_remove_wishlist) : getString(R.string.profile_add_wishlist));
            });
            loginButton.setOnClickListener(v -> {
                if (getActivity() != null) {
                    MainActivity mainActivity = (MainActivity) getActivity();
                    mainActivity.navigate(R.id.profile_navigation, true);
                }
            });

            Log.d(TAG, "onViewCreated: observable running");
            if (game == null) {
                return;
            }
            // Game cover
            RequestOptions requestOption = new RequestOptions().transform(new RoundedCorners(
                    (int) getResources().getDimension(R.dimen.big_cover_radius)
            ));
            String cover = game.getCover().getImageId();
            Glide.with(requireContext())
                    .load(game.hasCover() ? ImageBuilderKt.imageBuilder(cover, ImageSize.HD, ImageType.PNG) : null)
                    .apply(requestOption)
                    .placeholder(R.drawable.cover_placeholder_big)
                    .fallback(R.drawable.cover_placeholder_big)
                    .into(gameCoverImageView);

            // Game screenshots
            if (game.getScreenshotsCount() > 0) {
                screenshotContainer.setVisibility(View.VISIBLE);
                List<Screenshot> screenshots = game.getScreenshotsList();
                screenshotAdapter.submitList(screenshots);
            }

            // Game name
            gameNameTextView.setText(game.getName().toUpperCase());

            // Game ratings
            if (game.getRatingCount() > 0 && game.getRating() != 0) {
                //userRatingProgressBar.setProgress((int) game.getRating());
                if (userRatingProgressBar.getProgress() == 0) {
                    ObjectAnimator animation = ObjectAnimator
                            .ofInt(userRatingProgressBar, "progress", 0, (int) game.getRating());
                    animation.setDuration(500);
                    animation.start();
                }
                //Log.d(TAG, "game: " + game.getName() + ", user_rating: " + userRatingProgressBar.getProgress());
                gameUsersRatingText.setText(String.valueOf((int) floor(game.getRating())));
            } else {
                userRatingProgressBar.setProgress(0);
                //Log.d(TAG, "game: " + game.getName() + ", user_rating: " + userRatingProgressBar.getProgress());
                gameUsersRatingText.setText("N/A");
            }
            userText.setText(getString(R.string.user_text) + " (" + game.getRatingCount() + ")");

            if (game.getAggregatedRatingCount() > 0 && game.getAggregatedRating() != 0) {
                //criticRatingProgressBar.setProgress((int) game.getAggregatedRating());
                if (criticRatingProgressBar.getProgress() == 0) {

                    ObjectAnimator animation = ObjectAnimator
                            .ofInt(criticRatingProgressBar, "progress", 0, (int) game.getAggregatedRating());
                    animation.setDuration(500);
                    animation.start();
                }
                //Log.d(TAG, "game: " + game.getName() + ", critic_rating: " + criticRatingProgressBar.getProgress());
                gameCriticsRatingText.setText(String.valueOf((int) floor(game.getAggregatedRating())));
            } else {
                criticRatingProgressBar.setProgress(0);
                //Log.d(TAG, "game: " + game.getName() + ", critic_rating: " + criticRatingProgressBar.getProgress());
                gameCriticsRatingText.setText("N/A");
            }
            criticText.setText(getString(R.string.critics_text) + " (" + game.getAggregatedRatingCount() + ")");

            // Game developer & publisher
            List<Pair<Long, String>> developerList = new ArrayList<>();
            List<Pair<Long, String>> publisherList = new ArrayList<>();
            if (game.getInvolvedCompaniesCount() > 0) {
                for (int i = 0; i < game.getInvolvedCompaniesCount(); i++) {
                    InvolvedCompany involvedCompany = game.getInvolvedCompanies(i);
                    if (involvedCompany != null && involvedCompany.getCompany() != null) {
                        if (involvedCompany.getDeveloper()) {
                            Log.d(TAG, "onViewCreated: Developer company " + involvedCompany.getCompany().getName() + " - " + involvedCompany.getCompany().getId());
                            developerList.add(Pair.create(involvedCompany.getCompany().getId(), involvedCompany.getCompany().getName()));
                        }
                        if (involvedCompany.getPublisher()) {
                            Log.d(TAG, "onViewCreated: Publisher company " + involvedCompany.getCompany().getName() + " - " + involvedCompany.getCompany().getId());
                            publisherList.add(Pair.create(involvedCompany.getCompany().getId(), involvedCompany.getCompany().getName()));
                        }
                    }
                }
            }
            if (!developerList.isEmpty() && gameDeveloperRecyclerView.getAdapter() != null) {
                developerAdapter.submitList(developerList);
                developerContainer.setVisibility(View.VISIBLE);
            }
            if (!publisherList.isEmpty() && gamePublisherRecyclerView.getAdapter() != null) {
                publisherAdapter.submitList(publisherList);
                publisherContainer.setVisibility(View.VISIBLE);
            }

            // Game release
            if (game.hasFirstReleaseDate()) {
                long unixSeconds = game.getFirstReleaseDate().getSeconds();
                Date date = new java.util.Date(unixSeconds * 1000L);
                String datePattern = "MMM dd, yyyy";
                String formattedDate;
                if (Locale.getDefault().getLanguage().equals(Locale.ITALIAN.getLanguage())) {
                    datePattern = "dd MMM, yyyy";
                    SimpleDateFormat sdf = new java.text.SimpleDateFormat(datePattern, Locale.forLanguageTag(Locale.getDefault().getLanguage()));
                    formattedDate = sdf.format(date);
                    formattedDate = formattedDate.substring(0, 3) +
                            formattedDate.substring(3, 4).toUpperCase() +
                            formattedDate.substring(4);
                } else {
                    SimpleDateFormat sdf = new java.text.SimpleDateFormat(datePattern, Locale.forLanguageTag(Locale.getDefault().getLanguage()));
                    formattedDate = sdf.format(date);
                    formattedDate = formattedDate.substring(0, 1).toUpperCase() + formattedDate.substring(1);
                }

                gameReleaseView.setText(formattedDate);
                releaseContainer.setVisibility(View.VISIBLE);

                gameReleaseView.setOnClickListener(v -> {
                    SimpleDateFormat sdfYear = new SimpleDateFormat("yyyy", Locale.forLanguageTag(Locale.getDefault().getLanguage()));
                    Bundle bundle = new Bundle();
                    bundle.putString(ResultsFragment.RESULT_TYPE_KEY, String.valueOf(ResultType.YEAR));
                    bundle.putString(ResultsFragment.STRING_PARAMETER_KEY, String.valueOf(game.getFirstReleaseDate().getSeconds()));
                    bundle.putString(ResultsFragment.STRING_DISPLAYED_NAME_KEY, sdfYear.format(date));
                    Navigation.findNavController(v).navigate(R.id.fragment_results, bundle,  new NavOptions.Builder()
                            .setEnterAnim(R.anim.slide_in)
                            .setExitAnim(android.R.anim.fade_out)
                            .setPopEnterAnim(android.R.anim.fade_in)
                            .setPopExitAnim(R.anim.slide_out)
                            .build());
                });
            }

            // Game description
            final int summaryDisplayedLines = getResources().getInteger(R.integer.game_summary_max_lines);
            String summary = game.getSummary();
            if (summary != null && !summary.isEmpty()) {
                descriptionContainer.setVisibility(View.VISIBLE);
                gameDescriptionView.setMaxLines(Integer.MAX_VALUE);
                gameDescriptionView.setText(summary);
                final int lines = gameDescriptionView.getLineCount();
                gameDescriptionView.setMaxLines(summaryDisplayedLines);
                gameDescriptionView.setOnClickListener(v -> {
                    TextView textView = (TextView) v;
                    if (textView.getLineCount() != lines) {
                        textView.setMaxLines(lines);
                    } else {
                        textView.setMaxLines(summaryDisplayedLines);
                    }
                });
            }

            // Game platform
            final List<Pair<Long, String>> platformList = new ArrayList<>();
            for (int i = 0; i < game.getPlatformsCount(); i++) {
                Platform platform = game.getPlatforms(i);
                if (platform != null) {
                    platformList.add(Pair.create(platform.getId(), platform.getName()));
                }
            }
            if (!platformList.isEmpty() && gamePlatformRecyclerView.getAdapter() != null) {
                platformAdapter.submitList(platformList);
                platformContainer.setVisibility(View.VISIBLE);
            }


            // Game Genre
            final List<DetailEnum> genreList = new ArrayList<>();
            for (int i = 0; i < game.getGenresCount(); i++) {
                Genre genre = game.getGenres(i);
                if (genre != null) {
                    Genres genreEnum = Genres.valueOf(genre);
                    if (genreEnum != null) {
                        genreList.add(genreEnum);
                    }
                }
            }
            if (!genreList.isEmpty() && gameGenreRecyclerView.getAdapter() != null) {
                genresAdapter.submitList(genreList);
                genreContainer.setVisibility(View.VISIBLE);
            }

            // Game Themes
            final List<DetailEnum> themeList = new ArrayList<>();
            for (int i = 0; i < game.getThemesCount(); i++) {
                Theme theme = game.getThemes(i);
                if (theme != null) {
                    Themes themeEnum = Themes.valueOf(theme);
                    if (themeEnum != null) {
                        themeList.add(themeEnum);
                    }
                }
            }
            if (!themeList.isEmpty() && gameThemeRecyclerView.getAdapter() != null) {
                themesAdapter.submitList(themeList);
                themeContainer.setVisibility(View.VISIBLE);
            }
            // Game similar games
            if (game.getSimilarGamesCount() > 0) {
                if (similarGamesRecycler.getItemDecorationCount() == 0) {
                    CoverImageItemDecoration coverImageItemDecoration =
                            new CoverImageItemDecoration(
                                    getResources().getDimensionPixelSize(R.dimen.horizontal_cover_margin),
                                    getResources().getDimensionPixelSize(R.dimen.horizontal_cover_spacing),
                                    getResources().getDimensionPixelSize(R.dimen.horizontal_cover_margin)
                            );
                    similarGamesRecycler.addItemDecoration(coverImageItemDecoration);
                }
                List<Game> similarGamesList = game.getSimilarGamesList();
                viewModel.getSimilarGames(similarGamesList).observe(getViewLifecycleOwner(), gameList -> {
                    if (gameList.size() > 0) {
                        similarGamesContainer.setVisibility(View.VISIBLE);
                        coverAdapter.setToastManager(new ToastManager(requireContext()));
                        coverAdapter.setOnGameClickedListener(getOnGameClickedListener());
                        coverAdapter.submitList(gameList);
                    }
                });
            }
        };

        if (openFromIntent && !gameUrlPath.isEmpty()) {
            viewModel.getGame(gameUrlPath).observe(getViewLifecycleOwner(), observer);
        } else if (gameId != 0) {
            viewModel.getGame(gameId).observe(getViewLifecycleOwner(), observer);
        }
        viewModel.getIsLoadingLiveData().observe(getViewLifecycleOwner(), isLoading -> {
            loadingProgressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
        });
    }

    private FlexboxLayoutManager getFlexLayoutManager() {
        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(requireContext(), FlexDirection.ROW);
        layoutManager.setJustifyContent(JustifyContent.FLEX_START);
        layoutManager.setAlignItems(AlignItems.FLEX_START);
        return layoutManager;
    }

    private void addRecyclerDecoration(RecyclerView recyclerView, View view) {
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        int width = view.getMeasuredWidth();
        if (width != 0) {
            recyclerView.addItemDecoration(
                    new GameDetailItemDecoration(width + (int) getResources().getDimension(R.dimen.game_detail_text_space)));
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    private void setButtonsDimensions(Button b1, Button b2) {
        b1.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        b2.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        int b1Width = b1.getMeasuredWidth();
        int b2Width = b2.getMeasuredWidth();
        if (b1Width != 0 && b2Width != 0) {
            if (b1Width >= b2Width) {
                b2.setWidth(b1Width);
            } else {
                b1.setWidth(b2Width);
            }
        }
    }
}
