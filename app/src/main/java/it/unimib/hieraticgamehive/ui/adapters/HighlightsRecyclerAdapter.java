package it.unimib.hieraticgamehive.ui.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Map;
import java.util.Collections;
import java.util.List;

import it.unimib.hieraticgamehive.R;
import it.unimib.hieraticgamehive.services.ToastManager;
import it.unimib.hieraticgamehive.ui.HorizontalCoverView;
import it.unimib.hieraticgamehive.ui.interfaces.OnGameClickedListener;
import it.unimib.hieraticgamehive.utils.Highlights;
import proto.Game;

public class HighlightsRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final static int HEADER_VIEW_TYPE = 0;
    private final static int HIGHLIGHT_VIEW_TYPE = 1;
    private final static String TAG = "HighlightsRecyclerAdapter";
    private final Context context;
    private final Map<Integer, List<Game>> highlights;
    private final ToastManager toastManager;
    private final OnGameClickedListener onGameClickedListener;

    public HighlightsRecyclerAdapter(Context context, Map<Integer, List<Game>> highlights) {
        this(context, highlights, null);
    }

    public HighlightsRecyclerAdapter(
            Context context,
            Map<Integer, List<Game>> highlights,
            OnGameClickedListener onGameClickedListener
    ) {
        this(context, highlights, new ToastManager(context), onGameClickedListener);
    }

    private HighlightsRecyclerAdapter(
            Context context,
            Map<Integer, List<Game>> highlights,
            ToastManager toastManager,
            OnGameClickedListener onGameClickedListener
    ) {
        this.context = context;
        this.highlights = highlights;
        this.toastManager = toastManager;
        this.onGameClickedListener = onGameClickedListener;
    }

    public static class HighlightsViewHolder extends RecyclerView.ViewHolder {
        private final HorizontalCoverView horizontalCoverView;

        public HighlightsViewHolder(@NonNull View view) {
            super(view);
            horizontalCoverView = (HorizontalCoverView) view;
        }

        public HorizontalCoverView getHorizontalCoverView() {
            return horizontalCoverView;
        }
    }

    public static class HomeHeaderViewHolder extends RecyclerView.ViewHolder {

        public HomeHeaderViewHolder(@NonNull View view) {
            super(view);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        if (viewType == HEADER_VIEW_TYPE) {
            View view = LayoutInflater.from(context).inflate(R.layout.home_header, viewGroup, false);
            return new HomeHeaderViewHolder(view);
        }
        HorizontalCoverView coverView = new HorizontalCoverView(context, null);
        return new HighlightsViewHolder(coverView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof HomeHeaderViewHolder) {
            return;
        }
        else if (!(holder instanceof HighlightsViewHolder)) {
            return;
        }
        position--;
        HorizontalCoverView horizontalCoverView = ((HighlightsViewHolder) holder).getHorizontalCoverView();
        Highlights highlight = Highlights.values()[position];
        List<Game> gameList = highlights.getOrDefault(position, Collections.emptyList());
        if (gameList == null || gameList.size() == 0) {
            Log.d(TAG, "onBindViewHolder: ERROR empty list for highlight number " + position + " (" + highlight.toString() + ")");
            horizontalCoverView.setVisibility(View.GONE);
            return;
        }
        horizontalCoverView.setHighlightValue(highlight);
        horizontalCoverView.setHighlightText(highlight.getTitleId());
        horizontalCoverView.setToastManager(toastManager);
        if (onGameClickedListener != null) {
            horizontalCoverView.setOnGameClickedListener(onGameClickedListener);
        }
        horizontalCoverView.submitList(gameList);
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return HEADER_VIEW_TYPE;
        }
        return HIGHLIGHT_VIEW_TYPE;
    }

    @Override
    public int getItemCount() {
        return highlights.size() + 1;
    }
}
