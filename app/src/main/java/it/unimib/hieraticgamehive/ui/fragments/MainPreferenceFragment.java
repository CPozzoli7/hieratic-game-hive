package it.unimib.hieraticgamehive.ui.fragments;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.SwitchPreference;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;

import org.jetbrains.annotations.NotNull;

import java.util.Locale;

import it.unimib.hieraticgamehive.BuildConfig;
import it.unimib.hieraticgamehive.R;
import it.unimib.hieraticgamehive.core.MainActivity;
import it.unimib.hieraticgamehive.services.ToastManager;
import it.unimib.hieraticgamehive.viewmodels.ProfileViewModel;
import it.unimib.hieraticgamehive.viewmodels.factories.RepositoryViewModelFactory;

public class MainPreferenceFragment extends PreferenceFragmentCompat {

    private final String TAG = "MainPreferenceFragment";
    private ToastManager toastManager;
    private int version_click = 0;

    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        RepositoryViewModelFactory viewModelFactory = ((MainActivity) requireActivity()).getAppContainer().repositoryViewModelFactory;
        ProfileViewModel viewModel = new ViewModelProvider(requireActivity(), viewModelFactory).get(ProfileViewModel.class);

        viewModel.getUserLiveData().observe(getViewLifecycleOwner(), firebaseUser -> {
            if (firebaseUser != null) {
                Preference usernamePreference = findPreference("account_name");
                if (usernamePreference != null) {
                    usernamePreference.setSummary(firebaseUser.getDisplayName());
                    usernamePreference.setEnabled(true);
                }
                Preference emailPreference = findPreference("account_email");
                if (emailPreference != null) {
                    emailPreference.setSummary(firebaseUser.getEmail());
                    emailPreference.setEnabled(true);
                }
                Preference signOut = findPreference("account_sign_out");
                if (signOut != null) {
                    signOut.setEnabled(true);
                    signOut.setOnPreferenceClickListener(preference -> {
                        FirebaseAuth.getInstance().signOut();
                        if (getView() != null) {
                            Navigation.findNavController(getView()).navigate(R.id.profileContainerFragment);
                            return true;
                        } else {
                            return false;
                        }
                    });
                }
            }
        });

        return view;
    }

    @Override
    public RecyclerView onCreateRecyclerView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        RecyclerView recyclerView = super.onCreateRecyclerView(inflater, parent, savedInstanceState);
        recyclerView.setOverScrollMode(View.OVER_SCROLL_NEVER);
        recyclerView.setNestedScrollingEnabled(false);
        return recyclerView;
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.preferences, rootKey);

        Preference version = findPreference("version");
        if (version != null) {
            version.setSummary(BuildConfig.VERSION_NAME);
            version.setOnPreferenceClickListener(preference -> {
                if (++version_click == 5) {
                    Log.d(TAG, "onCreatePreferences: Clicked 5 times");
                    if (getActivity() != null && getView() != null) {
                        Navigation.findNavController(getView()).navigate(R.id.doggo_fragment);
                    }
                } else {
                    if (toastManager == null) {
                        toastManager = new ToastManager(getContext());
                    }
                    int count = 5 - version_click;
                    toastManager.displayShortToast(getResources().getQuantityString(R.plurals.version_click_multiples, count, count));
                }
                return true;
            });
        }

        // language
        ListPreference languagePreference = findPreference("language");
        if (languagePreference != null) {
            languagePreference.setOnPreferenceChangeListener((preference, newValue) -> {
                Locale locale;
                String value = (String) newValue;
                if (value.equals(getResources().getString(R.string.language_default_entry))) {
                    locale = Resources.getSystem().getConfiguration().getLocales().get(0);
                } else {
                    locale = new Locale(value);
                }
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.setLocale(locale);
                requireContext().getResources().updateConfiguration(config, null);
                requireActivity().recreate();
                return true;
            });
        }

        // theme
        SwitchPreference automaticTheme = findPreference("automatic_theme");
        if (automaticTheme != null) {
            final SwitchPreference darkMode = findPreference("dark_mode");
            if (darkMode != null) {
                darkMode.setChecked(isNightModeEnabledInPhone());
                darkMode.setEnabled(!automaticTheme.isChecked());
                darkMode.setOnPreferenceChangeListener((preference, newValue) -> {
                    boolean value = (boolean) newValue;
                    AppCompatDelegate.setDefaultNightMode(value ? AppCompatDelegate.MODE_NIGHT_YES : AppCompatDelegate.MODE_NIGHT_NO);
                    return true;
                });
            }
            automaticTheme.setOnPreferenceChangeListener((preference, newValue) -> {
                boolean value = (boolean) newValue;
                if (darkMode != null) {
                    if (value) {
                        AppCompatDelegate.setDefaultNightMode(
                                Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q ? AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM : AppCompatDelegate.MODE_NIGHT_AUTO_BATTERY
                        );
                    }
                    darkMode.setChecked(isNightModeEnabledInPhone());
                    darkMode.setEnabled(!value);
                }
                return true;
            });
        }

        //feedback
        Preference mailTo = findPreference("feedback");
        if (mailTo != null) {
            mailTo.setOnPreferenceClickListener(preference -> {
                Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:"));
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{
                        getResources().getString(R.string.email_a_maraschin),
                        getResources().getString(R.string.email_e_papa),
                        getResources().getString(R.string.email_c_pozzoli),
                        getResources().getString(R.string.email_m_radaelli),
                        getResources().getString(R.string.email_l_santambrogio)
                });
                intent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.feedback_email_object));
                Intent chooser = Intent.createChooser(intent, getResources().getString(R.string.feeback_open_with));
                if (getActivity() != null && chooser != null && chooser.resolveActivity(getActivity().getPackageManager()) != null) {
                    startActivity(chooser);
                }
                return true;
            });
        }
    }

    private boolean isNightModeEnabledInPhone() {
        return (getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK) == Configuration.UI_MODE_NIGHT_YES;
    }
}
