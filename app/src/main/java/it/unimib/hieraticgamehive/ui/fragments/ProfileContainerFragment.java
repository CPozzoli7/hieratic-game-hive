package it.unimib.hieraticgamehive.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.google.firebase.auth.FirebaseAuth;

import java.util.List;

import it.unimib.hieraticgamehive.R;
import it.unimib.hieraticgamehive.viewmodels.ProfileViewModel;

public class ProfileContainerFragment extends Fragment {

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_profile_container, container, false);
        if (FirebaseAuth.getInstance().getCurrentUser() == null) {
            addFragment(AuthenticationFragment.class);
        } else {
            addFragment(ProfileFragment.class);
        }
        return root;
    }

    private void addFragment(Class<? extends Fragment> fragmentClass) {
        List<Fragment> fragmentList = getChildFragmentManager().getFragments();
        if (fragmentList.size() == 0 || !fragmentList.get(0).getClass().isAssignableFrom(fragmentClass)) {
            getChildFragmentManager()
                    .beginTransaction()
                    .replace(R.id.profile_container_frame_layout, fragmentClass, null)
                    .commit();
        }
    }
}