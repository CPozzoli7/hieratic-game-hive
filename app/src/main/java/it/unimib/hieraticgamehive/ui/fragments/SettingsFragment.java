package it.unimib.hieraticgamehive.ui.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import it.unimib.hieraticgamehive.R;
import it.unimib.hieraticgamehive.core.MainActivity;
import it.unimib.hieraticgamehive.viewmodels.ProfileViewModel;
import it.unimib.hieraticgamehive.viewmodels.factories.RepositoryViewModelFactory;

public class SettingsFragment extends Fragment {
    //private TextView versionTextView;
    private ImageView upButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_settings, container, false);
//        RepositoryViewModelFactory viewModelFactory = ((MainActivity) requireActivity()).getAppContainer().repositoryViewModelFactory;
//        ProfileViewModel viewModel = new ViewModelProvider(requireActivity(), viewModelFactory).get(ProfileViewModel.class);

        upButton = root.findViewById(R.id.settings_up_button);
        upButton.setOnClickListener(v -> {
            requireActivity().onBackPressed();
        });

        getChildFragmentManager()
                .beginTransaction()
                .replace(R.id.preference_container, MainPreferenceFragment.class, null)
                .commit();

//        versionTextView = root.findViewById(R.id.app_version);
//        versionTextView.setText(String.valueOf(BuildConfig.VERSION_NAME));
//
//
//        viewModel.getUserLiveData().observe(getViewLifecycleOwner(), user -> {
//            if (user != null) {
//                TextView mailTextView = root.findViewById(R.id.app_email);
//                mailTextView.setText(String.valueOf(user.getEmail()));
//
//                TextView nameTextView = root.findViewById(R.id.app_username);
//                nameTextView.setText(String.valueOf(user.getDisplayName()));
//            }
//        });
//
//        Button signOutButton = root.findViewById(R.id.sign_out_button);
//        signOutButton.setText(R.string.sign_out);
//        signOutButton.setOnClickListener(view -> {
//            FirebaseAuth.getInstance().signOut();
//            Navigation.findNavController(view).navigate(R.id.profileContainerFragment);
//        });

        return root;
    }

//    @Override
//    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
//        setPreferencesFromResource(R.xml.settings, rootKey);
//
//        ListPreference languagePreference = findPreference("language");
//        if(languagePreference != null) {
//            languagePreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
//                @Override
//                public boolean onPreferenceChange(Preference preference, Object newValue) {
//                    String languageOption = (String) newValue;
//
//                    return false;
//                }
//            });
//        }
//    }

}
