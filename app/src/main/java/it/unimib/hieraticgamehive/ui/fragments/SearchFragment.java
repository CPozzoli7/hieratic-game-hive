package it.unimib.hieraticgamehive.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavOptions;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.flexbox.AlignItems;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;

import it.unimib.hieraticgamehive.R;
import it.unimib.hieraticgamehive.ui.adapters.GenresRecyclerAdapter;
import it.unimib.hieraticgamehive.core.interfaces.ItemReselectedFragment;
import it.unimib.hieraticgamehive.utils.Genres;
import it.unimib.hieraticgamehive.utils.ResultType;
import it.unimib.hieraticgamehive.viewmodels.SearchViewModel;

public class SearchFragment extends Fragment implements ItemReselectedFragment {

    private SearchViewModel searchViewModel;
    private SearchView searchView;

    private RecyclerView genresRecyclerView;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        searchViewModel =
                new ViewModelProvider(this).get(SearchViewModel.class);
        View root = inflater.inflate(R.layout.fragment_search, container, false);

        genresRecyclerView = root.findViewById(R.id.vertical_genres_recycler);

        genresRecyclerView.setLayoutManager(getFlexLayoutManager());
        GenresRecyclerAdapter genresAdapter = new GenresRecyclerAdapter(requireContext());
        genresRecyclerView.setAdapter(genresAdapter);

        searchView = root.findViewById(R.id.search_view_box);
        searchView.setOnQueryTextFocusChangeListener((view, hasFocus) -> {
            if (hasFocus) {
                ((InputMethodManager) requireContext().getSystemService(Context.INPUT_METHOD_SERVICE)).
                        showSoftInput(
                                searchView.findFocus(),
                                InputMethodManager.RESULT_UNCHANGED_SHOWN
                        );
            }
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Bundle bundle = new Bundle();
                bundle.putString(ResultsFragment.RESULT_TYPE_KEY, ResultType.SEARCH.name());
                bundle.putString(ResultsFragment.STRING_PARAMETER_KEY, query);
                bundle.putString(ResultsFragment.STRING_DISPLAYED_NAME_KEY, query);
                Navigation.findNavController(searchView).navigate(R.id.genre_results_fragment, bundle, new NavOptions.Builder()
                        .setEnterAnim(R.anim.slide_in)
                        .setExitAnim(android.R.anim.fade_out)
                        .setPopEnterAnim(android.R.anim.fade_in)
                        .setPopExitAnim(R.anim.slide_out)
                        .build());
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        return root;
    }

    @Override
    public void onItemReselected() {
        if (searchView != null) {
            searchView.requestFocus();
        }
    }

    private FlexboxLayoutManager getFlexLayoutManager() {
        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(requireContext(), FlexDirection.ROW);
        layoutManager.setJustifyContent(JustifyContent.CENTER);
        layoutManager.setAlignItems(AlignItems.FLEX_START);
        return layoutManager;
    }

    @Override
    public void onDestroyView() {
        View view = requireActivity().getCurrentFocus();
        if (view != null) {
            view.clearFocus();
        }
        super.onDestroyView();
    }
}