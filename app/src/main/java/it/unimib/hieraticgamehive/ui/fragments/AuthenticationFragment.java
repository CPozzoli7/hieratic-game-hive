package it.unimib.hieraticgamehive.ui.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavOptions;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import com.firebase.ui.auth.AuthUI;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Arrays;
import java.util.List;

import it.unimib.hieraticgamehive.R;
import it.unimib.hieraticgamehive.core.MainActivity;

public class AuthenticationFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_authentication, container, false);
        final Button signInButton = root.findViewById(R.id.sign_in_button);
        List<AuthUI.IdpConfig> providers = Arrays.asList(
                new AuthUI.IdpConfig.EmailBuilder().build(),
                new AuthUI.IdpConfig.GoogleBuilder().build());

        ActivityResultLauncher<Intent> activityResult = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
            if (FirebaseAuth.getInstance().getCurrentUser() != null) {
                if (getActivity() != null && ((MainActivity) getActivity()).navigateToSavedPosition()) {
                    return;
                }
                getParentFragmentManager()
                        .beginTransaction()
                        .replace(container.getId(), ProfileFragment.class, null)
                        .remove(this)
                        .commit();
            }
        });
        signInButton.setOnClickListener(view -> {
            activityResult.launch(AuthUI.getInstance()
                    .createSignInIntentBuilder()
                    .setAvailableProviders(providers)
                    .setIsSmartLockEnabled(false)
                    .setLogo(R.drawable.ic_honeycomb_firebase_authentication_logo)
                    .setTheme(R.style.Theme_FirebaseAuthenticationTheme)
                    .build());
        });

        final ImageButton profileSettingButton = root.findViewById(R.id.settings_imagebutton);
        profileSettingButton.setOnClickListener(v -> Navigation.findNavController(v).navigate(R.id.settingsFragment, null, new NavOptions.Builder()
                .setEnterAnim(R.anim.slide_in)
                .setExitAnim(android.R.anim.fade_out)
                .setPopEnterAnim(android.R.anim.fade_in)
                .setPopExitAnim(R.anim.slide_out)
                .build()));
        return root;
    }
}