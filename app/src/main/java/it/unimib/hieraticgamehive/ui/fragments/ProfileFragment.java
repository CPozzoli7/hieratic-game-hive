package it.unimib.hieraticgamehive.ui.fragments;

import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavOptions;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.flexbox.AlignItems;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayoutManager;

import org.jetbrains.annotations.NotNull;

import it.unimib.hieraticgamehive.R;
import it.unimib.hieraticgamehive.core.MainActivity;
import it.unimib.hieraticgamehive.core.interfaces.ItemReselectedFragment;
import it.unimib.hieraticgamehive.ui.adapters.CoverRecyclerAdapter;
import it.unimib.hieraticgamehive.viewmodels.ProfileViewModel;
import it.unimib.hieraticgamehive.viewmodels.factories.RepositoryViewModelFactory;


public class ProfileFragment extends GameFragment<ProfileViewModel> implements ItemReselectedFragment {
    private NestedScrollView scrollView;
    private RecyclerView collectionRecycler;
    private RecyclerView wishlistRecycler;
    private TextView emptyCollectionText;
    private TextView emptyWishlistText;
    private View collectionContainer;
    private View wishlistContainer;
    private Button collectionButton;
    private Button wishlistButton;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View root = inflater.inflate(R.layout.fragment_profile_menu, container, false);

        RepositoryViewModelFactory viewModelFactory = ((MainActivity) requireActivity()).getAppContainer().repositoryViewModelFactory;
        setViewModel(new ViewModelProvider(requireActivity(), viewModelFactory).get(ProfileViewModel.class));

        final ImageView image = root.findViewById(R.id.profile_icon);
        TextView profileUsername = root.findViewById(R.id.settings_username);

        viewModel.getUserLiveData().observe(getViewLifecycleOwner(), user -> {
            if (user != null) {
                Uri photoUri = user.getPhotoUrl();
                Glide.with(requireContext()).load(photoUri).into(image);

                profileUsername.setText(String.valueOf(user.getDisplayName()));
            }
        });

        scrollView = root.findViewById(R.id.user_scroll_view);

        // Button press
        collectionButton = root.findViewById(R.id.collection_button);
        wishlistButton = root.findViewById(R.id.wishlist_button);

        emptyCollectionText = root.findViewById(R.id.empty_collection);
        emptyWishlistText = root.findViewById(R.id.empty_wishlist);

        //collectionButton.getViewTreeObserver().addOnGlobalLayoutListener(getGlobalLayoutListenerButton(collectionButton, wishlistButton));

        wishlistContainer = root.findViewById(R.id.wishlist_container);
        collectionContainer = root.findViewById(R.id.collection_container);

        boolean collectionSelected = viewModel.isCollectionSelected();
        collectionButton.setSelected(collectionSelected);
        collectionContainer.setVisibility(collectionSelected ? View.VISIBLE : View.GONE);
        wishlistButton.setSelected(!collectionSelected);
        wishlistContainer.setVisibility(collectionSelected ? View.GONE : View.VISIBLE);

        collectionButton.setText(R.string.profile_collection);
        wishlistButton.setText(R.string.profile_wishlist);

        collectionRecycler = root.findViewById(R.id.profile_collection_recycler);
        collectionRecycler.setLayoutManager(getLayoutManager());
        CoverRecyclerAdapter gameCollectionAdapter = new CoverRecyclerAdapter(requireContext());
        gameCollectionAdapter.setOnGameClickedListener(getOnGameClickedListener());
        collectionRecycler.setAdapter(gameCollectionAdapter);

        wishlistRecycler = root.findViewById(R.id.profile_wishlist_recycler);
        wishlistRecycler.setLayoutManager(getLayoutManager());
        CoverRecyclerAdapter gameWishlistAdapter = new CoverRecyclerAdapter(requireContext());
        gameWishlistAdapter.setOnGameClickedListener(getOnGameClickedListener());
        wishlistRecycler.setAdapter(gameWishlistAdapter);

        int screenWidth = getScreenWidth();
        int coverWidth = getCoverWidth();
        int spanCount = getSpanCount();
        int freePixels = getFreePixels();
        int decorationSpacing = freePixels / (spanCount + 1);
        if (collectionRecycler.getItemDecorationCount() == 0) {
            collectionRecycler.addItemDecoration(new CoverDecoration(decorationSpacing));
        }
        if (wishlistRecycler.getItemDecorationCount() == 0) {
            wishlistRecycler.addItemDecoration(new CoverDecoration(decorationSpacing));
        }


        // Collection
        viewModel.getCollectionLiveData().observe(getViewLifecycleOwner(), games -> {
            if (games != null && games.size() == 0) {
                emptyCollectionText.setVisibility(View.VISIBLE);
                collectionRecycler.setVisibility(View.GONE);
            } else {
                emptyCollectionText.setVisibility(View.GONE);
                collectionRecycler.setVisibility(View.VISIBLE);
            }
            gameCollectionAdapter.submitList(games);
        });

        // Wishlist
        viewModel.getWishlistLiveData().observe(getViewLifecycleOwner(), games -> {
            if (games != null && games.size() == 0) {
                emptyWishlistText.setVisibility(View.VISIBLE);
                wishlistRecycler.setVisibility(View.GONE);
            } else {
                emptyWishlistText.setVisibility(View.GONE);
                wishlistRecycler.setVisibility(View.VISIBLE);
            }
            gameWishlistAdapter.submitList(games);
        });

        collectionButton.setOnClickListener(v -> {
            viewModel.setCollectionSelected(true);
            collectionButton.setSelected(true);
            wishlistButton.setSelected(false);
            wishlistContainer.setVisibility(View.GONE);
            collectionContainer.setVisibility(View.VISIBLE);
        });

        wishlistButton.setOnClickListener(v -> {
            viewModel.setCollectionSelected(false);
            wishlistButton.setSelected(true);
            collectionButton.setSelected(false);
            collectionContainer.setVisibility(View.GONE);
            wishlistContainer.setVisibility(View.VISIBLE);
        });

        final ImageButton profileSettingButton = root.findViewById(R.id.settings_imagebutton);
        profileSettingButton.setOnClickListener(v -> Navigation.findNavController(v).navigate(R.id.settingsFragment, null, new NavOptions.Builder()
                .setEnterAnim(R.anim.slide_in)
                .setExitAnim(android.R.anim.fade_out)
                .setPopEnterAnim(android.R.anim.fade_in)
                .setPopExitAnim(R.anim.slide_out)
                .build()));

        return root;
    }

    private int getCoverWidth() {
        float width = 0;
        if (getContext() != null) {
            width = getContext().getResources().getDimension(R.dimen.small_cover_width);
        }
        return (int) width;
    }

    private int getScreenWidth() {
        int width = 0;
        if (getActivity() != null) {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            width = displayMetrics.widthPixels;
        }
        return width;
    }

    private int getSpanCount() {
        int spanCount = getScreenWidth() / getCoverWidth();
        int extraSpanCount = 0;
//        if (getFreePixels() / (spanCount + 1) < 4) {
//            extraSpanCount++;
//        }
        return spanCount - extraSpanCount;
    }

    private int getFreePixels() {
        return getScreenWidth() % getCoverWidth();
    }

    private RecyclerView.LayoutManager getLayoutManager() {
        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(requireContext(), FlexDirection.ROW);
        layoutManager.setAlignItems(AlignItems.FLEX_START);
        return layoutManager;
    }

    @Override
    public void onItemReselected() {
        if (scrollView != null) {
            scrollView.smoothScrollTo(0, 0);
        }
    }

    private static class CoverDecoration extends RecyclerView.ItemDecoration {
        private final int spacing;

        public CoverDecoration(int spacing) {
            this.spacing = spacing;
        }

        @Override
        public void getItemOffsets(@NonNull @NotNull Rect outRect, @NonNull @NotNull View view, @NonNull @NotNull RecyclerView parent, @NonNull @NotNull RecyclerView.State state) {
            outRect.left = spacing;
            outRect.bottom = spacing;
        }
    }
}
