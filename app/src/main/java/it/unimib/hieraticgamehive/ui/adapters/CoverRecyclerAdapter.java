package it.unimib.hieraticgamehive.ui.adapters;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.api.igdb.utils.ImageBuilderKt;
import com.api.igdb.utils.ImageSize;
import com.api.igdb.utils.ImageType;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;

import it.unimib.hieraticgamehive.R;
import it.unimib.hieraticgamehive.services.ToastManager;
import it.unimib.hieraticgamehive.ui.fragments.GameDetailFragment;
import it.unimib.hieraticgamehive.ui.interfaces.OnGameClickedListener;
import proto.Game;

public class CoverRecyclerAdapter extends ListAdapter<Game, CoverRecyclerAdapter.CoverViewHolder> {
    private final Context context;
    private ToastManager toastManager;
    private OnGameClickedListener onGameClickedListener = null;

    public CoverRecyclerAdapter(Context context) {
        super(new DiffUtil.ItemCallback<Game>() {
            @Override
            public boolean areItemsTheSame(@NonNull Game oldItem, @NonNull Game newItem) {
                return oldItem.getId() == newItem.getId();
            }

            @Override
            public boolean areContentsTheSame(@NonNull Game oldItem, @NonNull Game newItem) {
                return oldItem.getId() == newItem.getId();
            }
        });
        this.context = context;
    }

    public CoverRecyclerAdapter(Context context, DiffUtil.ItemCallback<Game> diffUtil) {
        super(diffUtil);
        this.context = context;
    }

    public void setOnGameClickedListener(OnGameClickedListener onCoverClickedListener) {
        this.onGameClickedListener = onCoverClickedListener;
    }

    public void setToastManager(ToastManager toastManager) {
        this.toastManager = toastManager;
    }

    public static class CoverViewHolder extends RecyclerView.ViewHolder {
        private final ImageView coverImageView;

        public CoverViewHolder(@NonNull View view) {
            super(view);
            coverImageView = view.findViewById(it.unimib.hieraticgamehive.R.id.cover_image_view);
        }

        public ImageView getCoverImageView() {
            return coverImageView;
        }
    }

    @NonNull
    @Override
    public CoverViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.single_cover_image, viewGroup, false);
        return new CoverViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CoverViewHolder viewHolder, int position) {
        if (getCurrentList().isEmpty()) {
            return;
        }
        Game game = getCurrentList().get(position);
        if (game == null) {
            return;
        }
        String imageID = game.getCover().getImageId();
        RequestOptions requestOptions = new RequestOptions().transform(new RoundedCorners(
                (int) context.getResources().getDimension(R.dimen.small_cover_radius)
        ));
        Glide.with(context)
                .load(game.hasCover() ? ImageBuilderKt.imageBuilder(imageID, ImageSize.COVER_BIG, ImageType.JPEG) : null)
                .apply(requestOptions)
                .placeholder(R.drawable.cover_placeholder_big)
                .fallback(R.drawable.cover_placeholder_big)
                .into(viewHolder.getCoverImageView());

        viewHolder.getCoverImageView().setOnClickListener(v -> {
            if (onGameClickedListener == null) {
                Bundle bundle = new Bundle();
                bundle.putLong(GameDetailFragment.LONG_GAME_ID_KEY, game.getId());
                Navigation.findNavController(v).navigate(R.id.fragment_detail, bundle);
            } else {
                onGameClickedListener.onGameClicked(game);
            }
        });
        viewHolder.getCoverImageView().setOnLongClickListener(v -> {
            if (toastManager != null) {
                toastManager.displayShortToast(String.valueOf(game.getName()));
            }
            return true;
        });
    }

    @Override
    public int getItemCount() {
        getCurrentList();
        if (getCurrentList().isEmpty()) {
            return 0;
        }
        return getCurrentList().size();
    }
}
