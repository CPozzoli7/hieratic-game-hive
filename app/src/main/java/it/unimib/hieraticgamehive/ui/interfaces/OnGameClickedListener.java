package it.unimib.hieraticgamehive.ui.interfaces;

import proto.Game;

public interface OnGameClickedListener {
    void onGameClicked(Game game);
}
