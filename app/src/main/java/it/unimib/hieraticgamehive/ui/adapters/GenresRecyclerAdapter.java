package it.unimib.hieraticgamehive.ui.adapters;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.navigation.NavOptions;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import it.unimib.hieraticgamehive.R;
import it.unimib.hieraticgamehive.ui.fragments.ResultsFragment;
import it.unimib.hieraticgamehive.utils.Genres;
import it.unimib.hieraticgamehive.utils.ResultType;

public class GenresRecyclerAdapter
        extends RecyclerView.Adapter<GenresRecyclerAdapter.GenreViewHolder> {
    private final Genres[] genres = Genres.values();
    private final Context context;

    public GenresRecyclerAdapter(Context context) {
        this.context = context;
    }

    public static class GenreViewHolder extends RecyclerView.ViewHolder {
        private final Button button;

        public GenreViewHolder(@NonNull View view) {
            super(view);
            button = view.findViewById(R.id.genre_button);
        }

        public Button getButton() {
            return button;
        }
    }

    @NonNull
    @Override
    public GenreViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_genre_button, parent, false);
        return new GenreViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GenreViewHolder holder, int position) {
        holder.getButton().setText(genres[position].getTitleId());
        holder.getButton().setOnClickListener(view -> {
            Bundle bundle = new Bundle();
            bundle.putString(ResultsFragment.RESULT_TYPE_KEY, ResultType.GENRE.name());
            bundle.putString(ResultsFragment.STRING_DISPLAYED_NAME_KEY, context.getResources().getString(genres[position].getTitleId()));
            bundle.putString(ResultsFragment.STRING_PARAMETER_KEY, genres[position].name());
            Navigation.findNavController(view).navigate(R.id.genre_results_fragment, bundle, new NavOptions.Builder()
                    .setEnterAnim(R.anim.slide_in)
                    .setExitAnim(android.R.anim.fade_out)
                    .setPopEnterAnim(android.R.anim.fade_in)
                    .setPopExitAnim(R.anim.slide_out)
                    .build());
        });
    }

    @Override
    public int getItemCount() {
        return genres.length;
    }

}
