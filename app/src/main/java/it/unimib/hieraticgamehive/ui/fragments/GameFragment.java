package it.unimib.hieraticgamehive.ui.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.NavOptions;
import androidx.navigation.Navigation;

import it.unimib.hieraticgamehive.R;
import it.unimib.hieraticgamehive.ui.interfaces.OnGameClickedListener;
import it.unimib.hieraticgamehive.viewmodels.GameRepositoryViewModel;
import proto.Game;

public abstract class GameFragment<T extends GameRepositoryViewModel> extends Fragment {
    T viewModel;

    OnGameClickedListener getOnGameClickedListener() {
        return this::onGameClicked;
    }

    void setViewModel(T repositoryViewModel) {
        this.viewModel = repositoryViewModel;
    }

    void onGameClicked(Game game) {
        viewModel.setSelectedGame(game);
        Bundle bundle = new Bundle();
        bundle.putLong(GameDetailFragment.LONG_GAME_ID_KEY, game.getId());
        if (getView() != null) {
            Navigation.findNavController(getView()).navigate(
                    R.id.fragment_detail,
                    bundle,
                    new NavOptions.Builder()
                            .setEnterAnim(R.anim.slide_in)
                            .setExitAnim(android.R.anim.fade_out)
                            .setPopEnterAnim(android.R.anim.fade_in)
                            .setPopExitAnim(R.anim.slide_out)
                            .build()
            );
        }
    }
}
