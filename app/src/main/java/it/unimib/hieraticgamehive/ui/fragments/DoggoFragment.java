package it.unimib.hieraticgamehive.ui.fragments;

import android.content.res.Resources;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;
import org.w3c.dom.Text;

import it.unimib.hieraticgamehive.R;
import it.unimib.hieraticgamehive.services.ToastManager;

public class DoggoFragment extends Fragment {

    private ImageView upButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_doggo, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        upButton = view.findViewById(R.id.doggo_up_button);
        upButton.setOnClickListener(v -> {
            requireActivity().onBackPressed();
        });

        final ViewGroup doggoContainer = view.findViewById(R.id.doggo_container);
        doggoContainer.setOnClickListener(v -> {
            TextView textView = new TextView(requireContext());
            textView.setText(R.string.easter_egg);
            TypedValue value = new TypedValue();
            requireContext().getTheme().resolveAttribute(R.attr.MainTextColor, value, true);
            textView.setTextColor(value.data);
            textView.setTextSize((float) (Math.random() * 51) + 30);
            textView.setRotation((float) ((Math.random() * 91) - 45));
            textView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
            textView.setX((float) (Math.random() * (view.getWidth() - textView.getMeasuredWidth())));
            textView.setY((float) (Math.random() * (view.getHeight() - textView.getMeasuredHeight())));
            doggoContainer.addView(textView);
        });
    }
}