package it.unimib.hieraticgamehive.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.api.igdb.utils.ImageBuilderKt;
import com.api.igdb.utils.ImageSize;
import com.api.igdb.utils.ImageType;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;

import org.jetbrains.annotations.NotNull;

import it.unimib.hieraticgamehive.R;
import proto.Game;
import proto.Screenshot;

public class ScreenshotRecyclerAdapter extends ListAdapter<Screenshot, ScreenshotRecyclerAdapter.ScreenshotViewHolder> {
    private final Context context;

    public ScreenshotRecyclerAdapter(Context context) {
        super(new DiffUtil.ItemCallback<Screenshot>() {
            @Override
            public boolean areItemsTheSame(@NonNull Screenshot oldItem, @NonNull Screenshot newItem) {
                return oldItem.getId() == newItem.getId();
            }

            @Override
            public boolean areContentsTheSame(@NonNull Screenshot oldItem, @NonNull Screenshot newItem) {
                return oldItem.getId() == newItem.getId();
            }
        });
        this.context = context;
    }

    public static class ScreenshotViewHolder extends RecyclerView.ViewHolder {
        private final ImageView screenshotImageView;

        public ScreenshotViewHolder(@NonNull View view) {
            super(view);
            screenshotImageView = view.findViewById(R.id.screenshot_image_view);
        }

        public ImageView getScreenshotImageView() {
            return screenshotImageView;
        }
    }

    @NonNull
    @Override
    public ScreenshotViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.single_screenshot_image, viewGroup, false);
        return new ScreenshotViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ScreenshotViewHolder holder, int position) {
        if (getCurrentList().isEmpty()) {
            return;
        }
        String screenshotID= getCurrentList().get(position).getImageId();
        RequestOptions requestOptions = new RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .transform(new RoundedCorners(
                        (int) context.getResources().getDimension(R.dimen.big_cover_radius)
                ));
        Glide.with(context)
                .load(ImageBuilderKt.imageBuilder(screenshotID, ImageSize.SCREENSHOT_HUGE, ImageType.JPEG))
                .thumbnail(Glide.with(context)
                        .load(ImageBuilderKt.imageBuilder(screenshotID, ImageSize.SCREENSHOT_MEDIUM, ImageType.JPEG))
                        .apply(requestOptions))
                .apply(requestOptions)
                .into(holder.getScreenshotImageView());
    }

}
