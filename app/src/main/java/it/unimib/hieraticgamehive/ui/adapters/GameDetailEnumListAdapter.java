package it.unimib.hieraticgamehive.ui.adapters;

import android.content.Context;

import androidx.annotation.NonNull;

import it.unimib.hieraticgamehive.utils.DetailEnum;
import it.unimib.hieraticgamehive.utils.ResultType;

public class GameDetailEnumListAdapter extends GameDetailListAdapter<DetailEnum>{
    private final Context context;

    public GameDetailEnumListAdapter(Context context, @NonNull AdapterType adapterType, @NonNull ResultType resultType) {
        super(context, adapterType, resultType);
        this.context = context;
    }

    @Override
    String getDisplayedValue(int position) {
        return context.getResources().getString(getCurrentList().get(position).getTitleId());
    }

    @Override
    String getResultsParameterValue(int position) {
        return getCurrentList().get(position).name();
    }
}
