package it.unimib.hieraticgamehive.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import it.unimib.hieraticgamehive.R;
import it.unimib.hieraticgamehive.ui.adapters.CoverRecyclerAdapter;
import it.unimib.hieraticgamehive.decorations.CoverImageItemDecoration;
import it.unimib.hieraticgamehive.services.ToastManager;
import it.unimib.hieraticgamehive.ui.interfaces.OnGameClickedListener;
import it.unimib.hieraticgamehive.utils.Highlights;
import proto.Game;

public class HorizontalCoverView extends FrameLayout {

    private final RecyclerView coverRecycler;
    private final TextView highlightTextView;
    private final CoverRecyclerAdapter coverRecyclerAdapter;
    private Highlights highlight;
    private final ConstraintLayout horizontalCoverLayout;

    public HorizontalCoverView(Context context) {
        this(context, null);
    }

    public HorizontalCoverView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HorizontalCoverView(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public HorizontalCoverView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        View view = LayoutInflater.from(context).inflate(R.layout.horizontal_cover_view, this);
        horizontalCoverLayout = view.findViewById(R.id.horizontal_cover_layout);
        coverRecycler = view.findViewById(R.id.cover_recycler);
        highlightTextView = view.findViewById(R.id.highlight_text_view);
        CoverImageItemDecoration coverImageItemDecoration =
                new CoverImageItemDecoration(
                        getResources().getDimensionPixelSize(R.dimen.horizontal_cover_margin),
                        getResources().getDimensionPixelSize(R.dimen.horizontal_cover_spacing),
                        getResources().getDimensionPixelSize(R.dimen.horizontal_cover_margin)
                );
        coverRecycler.addItemDecoration(coverImageItemDecoration);
        coverRecyclerAdapter = new CoverRecyclerAdapter(context);
        coverRecycler.setAdapter(coverRecyclerAdapter);
    }

    public RecyclerView getCoverRecycler() {
        return coverRecycler;
    }

    public TextView getHighlightTextView() {
        return highlightTextView;
    }

    public Highlights getHighlightValue() {
        return highlight;
    }

    public void setHighlightValue(Highlights highlight) {
        this.highlight = highlight;
    }

    public void setHighlightText(int titleId) {
        highlightTextView.setText(titleId);
    }

    public void setHighlightText(String title) {
        highlightTextView.setText(title);
    }

    public void submitList(List<Game> games) {
        coverRecyclerAdapter.submitList(games);
    }

    public void setToastManager(ToastManager toastManager) {
        coverRecyclerAdapter.setToastManager(toastManager);
    }

    public void setOnGameClickedListener(OnGameClickedListener onGameClickedListener) {
        coverRecyclerAdapter.setOnGameClickedListener(onGameClickedListener);
    }

    @Override
    public void setVisibility(int visibility) {
        horizontalCoverLayout.setVisibility(visibility);
        super.setVisibility(visibility);
    }
}
