package it.unimib.hieraticgamehive.ui.adapters;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.api.igdb.utils.ImageBuilderKt;
import com.api.igdb.utils.ImageSize;
import com.api.igdb.utils.ImageType;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.card.MaterialCardView;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import it.unimib.hieraticgamehive.R;
import it.unimib.hieraticgamehive.services.ToastManager;
import it.unimib.hieraticgamehive.ui.fragments.GameDetailFragment;
import it.unimib.hieraticgamehive.ui.interfaces.OnGameClickedListener;
import it.unimib.hieraticgamehive.utils.SearchDetail;
import proto.Company;
import proto.Game;
import proto.InvolvedCompany;

public class ResultRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final static int HEADER_VIEW_TYPE = 0;
    private final static int RESULTS_VIEW_TYPE = 1;
    private final static int SEARCH_DETAIL_VIEW_TYPE = 2;
    private final static int LOADING_VIEW_TYPE = 3;

    private final static String TAG = "ResultRecyclerAdapter";
    private final Context context;
    private final String resultText;
    private ToastManager toastManager;
    private SearchDetail searchDetail;
    private List<Game> games;
    private final OnGameClickedListener onGameClickedListener;
    private final OnItemVisibleListener onItemVisibleListener;
    private OnSummaryClicked onSummaryClicked;
    private static final SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy", Locale.getDefault());

    public ResultRecyclerAdapter(Context context, String resultText) {
        this(context, resultText, null, new ToastManager(context), null, null);
    }

    public ResultRecyclerAdapter(Context context,
                                 String resultText,
                                 List<Game> games,
                                 OnGameClickedListener onGameClickedListener,
                                 OnItemVisibleListener onItemVisibleListener) {
        this(context, resultText, games, new ToastManager(context), onGameClickedListener, onItemVisibleListener);
    }

    public ResultRecyclerAdapter(Context context,
                                 String resultText,
                                 OnGameClickedListener onGameClickedListener,
                                 OnItemVisibleListener onItemVisibleListener) {
        this(context, resultText, null, new ToastManager(context), onGameClickedListener, onItemVisibleListener);
    }

    private ResultRecyclerAdapter(Context context,
                                  String resultText,
                                  List<Game> list,
                                  ToastManager toastManager,
                                  OnGameClickedListener onGameClickedListener,
                                  OnItemVisibleListener onItemVisibleListener) {
        this.context = context;
        this.resultText = resultText;
        this.toastManager = toastManager;
        this.games = list;
        this.onGameClickedListener = onGameClickedListener;
        this.onItemVisibleListener = onItemVisibleListener;
    }

    public interface OnItemVisibleListener {
        void onItemVisible();

        int getPositionFromBottom();
    }

    public interface OnSummaryClicked {
        void onExpand();

        void onCompress();
    }

    public void setToastManager(ToastManager toastManager) {
        this.toastManager = toastManager;
    }

    public void setSearchDetail(SearchDetail searchDetail, OnSummaryClicked onSummaryClicked) {
        this.searchDetail = searchDetail;
        notifyItemChanged(1);
        this.onSummaryClicked = onSummaryClicked;
    }

    public static class SingleResultViewHolder extends RecyclerView.ViewHolder {
        private final TextView resultText;
        private final ImageView resultCover;
        private final TextView resultPosition;
        private final TextView resultDeveloper;
        private final TextView resultRelease;

        private long currentGameId = 0;

        public SingleResultViewHolder(View view) {
            super(view);
            resultText = view.findViewById(R.id.name_result_text_view);
            resultCover = view.findViewById(R.id.single_result_cover_view);
            resultPosition = view.findViewById(R.id.result_position_text_view);
            resultDeveloper = view.findViewById(R.id.developer_result_text_view);
            resultRelease = view.findViewById(R.id.release_date__result_text_view);
        }

        public void bind(Game game, OnGameClickedListener onGameClickedListener) {
            if (currentGameId == game.getId()) {
                return;
            }
            currentGameId = game.getId();
            Log.d(TAG, "bind: BIND FOR GAME " + game.getName() + " " + game.getId());
            Log.d(TAG, "bind: name " + resultText.getText());
            Log.d(TAG, "bind: developer " + resultDeveloper.getText());
            Log.d(TAG, "bind: release " + resultRelease.getText());
            //getting covers
            String imageID = game.getCover().getImageId();
            String name = game.getName();
            String developer = "";
            String release = "";

            RequestOptions requestOptions = new RequestOptions().transform(new RoundedCorners(
                    (int) resultCover.getContext().getResources().getDimension(R.dimen.small_cover_radius)
            ));
            Glide.with(resultCover)
                    .load(game.hasCover() ? ImageBuilderKt.imageBuilder(imageID, ImageSize.COVER_BIG, ImageType.PNG) : null)
                    .apply(requestOptions)
                    .placeholder(R.drawable.cover_placeholder_big)
                    .fallback(R.drawable.cover_placeholder_big)
                    .into(resultCover);

            //getting result position
//        String gamePosition = (position + 1) + ".";
//        getResultPositionTextView().setText(gamePosition);

            //getting developer
//            resultDeveloper.setText("");
            boolean developerFound = false;
            for (int i = 0; i < game.getInvolvedCompaniesCount() && !developerFound; i++) {
                developerFound = game.getInvolvedCompanies(i).getDeveloper();
                if (developerFound) {
                    developer = game.getInvolvedCompanies(i).getCompany().getName();
                }
            }
            if (developerFound) {
                resultDeveloper.setVisibility(View.VISIBLE);
            } else {
                resultDeveloper.setVisibility(View.GONE);
            }

            //getting release_date
            if (game.hasFirstReleaseDate()) {
                long unixSeconds = game.getFirstReleaseDate().getSeconds();
                Date date = new java.util.Date(unixSeconds * 1000L);
                release = sdf.format(date);
                resultRelease.setVisibility(View.VISIBLE);
            } else {
                resultRelease.setVisibility(View.GONE);
            }

            resultText.setText(name);
            resultDeveloper.setText(developer);
            resultRelease.setText(release);

            itemView.setOnClickListener(v -> {
                if (onGameClickedListener == null) {
                    Bundle bundle = new Bundle();
                    bundle.putLong(GameDetailFragment.LONG_GAME_ID_KEY, game.getId());
                    Navigation.findNavController(v).navigate(R.id.fragment_detail, bundle);
                } else {
                    onGameClickedListener.onGameClicked(game);
                }
            });
        }
    }

    public static class ResultsHeaderViewHolder extends RecyclerView.ViewHolder {
        private final TextView resultTextView;

        public ResultsHeaderViewHolder(View view) {
            super(view);
            resultTextView = view.findViewById(R.id.search_result_text);
            ImageView upButton = view.findViewById(R.id.results_up_button);
            upButton.setOnClickListener(v -> {
                if (v.getContext() != null) {
                    ((Activity) v.getContext()).onBackPressed();
                }
            });
        }

        public void setText(String text) {
            resultTextView.setText(text);
        }
    }

    public static class LoadingViewHolder extends RecyclerView.ViewHolder {

        public LoadingViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
        }
    }

    public static class SearchDetailViewHolder extends RecyclerView.ViewHolder {
        private final MaterialCardView cardView;
        private final int contentPadding;
        private final ViewGroup viewGroup;
        private final ImageView image;
        private final TextView name;
        private final TextView date;
        private final TextView summary;

        public SearchDetailViewHolder(View view) {
            super(view);
            cardView = view.findViewById(R.id.search_detail_cardview);
            contentPadding = cardView.getContentPaddingLeft();
            viewGroup = view.findViewById(R.id.search_detail_constraintLayout);
            image = view.findViewById(R.id.search_detail_logo);
            name = view.findViewById(R.id.search_detail_name);
            date = view.findViewById(R.id.search_detail_date);
            summary = view.findViewById(R.id.search_detail_summary);
        }

        public void setDetails(SearchDetail searchDetail, OnSummaryClicked onSummaryClicked) {
            Glide.with(image.getContext())
                    .load(ImageBuilderKt.imageBuilder(searchDetail.getImageId(), ImageSize.LOGO_MEDIUM, ImageType.PNG))
                    .into(image);
            if (searchDetail.getName().isEmpty()) {
                name.setVisibility(View.GONE);
            } else {
                name.setText(searchDetail.getName());
            }
            if (searchDetail.getReleaseDate().isEmpty()) {
                date.setVisibility(View.GONE);
            } else {
                date.setText(searchDetail.getReleaseDate());
            }
            if (searchDetail.getSummary().isEmpty()) {
                summary.setVisibility(View.GONE);
            } else {
                final int summaryDisplayedLines = 5;
                summary.setMaxLines(summaryDisplayedLines);
                summary.setText(searchDetail.getSummary());
                summary.post(() -> {
                    cardView.setClickable(true);
                    cardView.setOnClickListener(v -> {
                        if (summary.getMaxLines() != Integer.MAX_VALUE) {
                            if (onSummaryClicked != null) {
                                onSummaryClicked.onExpand();
                            }
                            summary.setMaxLines(Integer.MAX_VALUE);
                        } else {
                            if (onSummaryClicked != null) {
                                onSummaryClicked.onCompress();
                            }
                            summary.setMaxLines(summaryDisplayedLines);
                        }
                    });
                });
            }
        }

        public void setVisibility(int visibility) {
            cardView.setVisibility(visibility);
            viewGroup.setVisibility(visibility);
            itemView.setVisibility(visibility);
            if (visibility == View.GONE) {
                cardView.setContentPadding(0, 0, 0, 0);
            } else {
                cardView.setContentPadding(contentPadding, contentPadding, contentPadding, contentPadding);
            }
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == HEADER_VIEW_TYPE) {
            View view = LayoutInflater.from(context).inflate(R.layout.results_header, parent, false);
            return new ResultsHeaderViewHolder(view);
        } else if (viewType == SEARCH_DETAIL_VIEW_TYPE) {
            View view = LayoutInflater.from(context).inflate(R.layout.search_detail_cardview, parent, false);
            return new SearchDetailViewHolder(view);
        } else if (viewType == LOADING_VIEW_TYPE) {
            View view = LayoutInflater.from(context).inflate(R.layout.loading_results_cardview, parent, false);
            return new LoadingViewHolder(view);
        }
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_result_cardview, parent, false);
        return new SingleResultViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ResultsHeaderViewHolder) {
            ResultsHeaderViewHolder headerViewHolder = ((ResultsHeaderViewHolder) holder);
            headerViewHolder.setText(resultText);

            return;
        } else if (holder instanceof SearchDetailViewHolder) {
            SearchDetailViewHolder searchDetailViewHolder = (SearchDetailViewHolder) holder;
            if (searchDetail != null) {
                searchDetailViewHolder.setDetails(searchDetail, onSummaryClicked);
                searchDetailViewHolder.setVisibility(View.VISIBLE);
            } else {
                searchDetailViewHolder.setVisibility(View.GONE);
            }
            return;
        } else if (!(holder instanceof SingleResultViewHolder) || games == null) {
            return;
        }
        int gamePosition = position - 2;
        if (onItemVisibleListener != null && getList() != null && gamePosition == getList().size() - onItemVisibleListener.getPositionFromBottom()) {
            onItemVisibleListener.onItemVisible();
        }
        Log.d(TAG, "onBindViewHolder: Started binding for game at position " + gamePosition);
        SingleResultViewHolder singleResultViewHolder = ((SingleResultViewHolder) holder);
        Game game = games.get(gamePosition);
        if (game == null) {
            if (position != getList().size() - 1) {
                //singleResultViewHolder.itemView.setVisibility(View.GONE);
            }
            return;
        }
        singleResultViewHolder.bind(game, onGameClickedListener);
        Log.d(TAG, "onBindViewHolder: Ended binding for game at position " + gamePosition);
    }

    public void setList(List<Game> list) {
        games = list;
    }

    public List<Game> getList() {
        return games;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return HEADER_VIEW_TYPE;
        } else if (position == 1) {
            return SEARCH_DETAIL_VIEW_TYPE;
        } else if (position - 2 == getList().size() - 1 && getList().get(position - 2) == null) {
            return LOADING_VIEW_TYPE;
        }
        return RESULTS_VIEW_TYPE;
    }

    @Override
    public int getItemCount() {
        if (games == null) {
            return 2;
        }
        return games.size() + 2;
    }
}
