package it.unimib.hieraticgamehive.ui.fragments;

import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.vectordrawable.graphics.drawable.Animatable2Compat;
import androidx.vectordrawable.graphics.drawable.AnimatedVectorDrawableCompat;

import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import it.unimib.hieraticgamehive.R;
import it.unimib.hieraticgamehive.utils.ResultType;
import it.unimib.hieraticgamehive.viewmodels.ResultsViewModel;
import it.unimib.hieraticgamehive.ui.adapters.ResultRecyclerAdapter;
import it.unimib.hieraticgamehive.core.MainActivity;
import it.unimib.hieraticgamehive.viewmodels.factories.RepositoryViewModelFactory;
import proto.Game;

public class ResultsFragment extends GameFragment<ResultsViewModel> {
    // TODO: Generalize for every query possible (games.name, genres.name, producers.name, ...)
    public static final String RESULT_TYPE_KEY = "RESULT_TYPE";
    public static final String STRING_PARAMETER_KEY = "STRING_PARAMETER";
    public static final String STRING_DISPLAYED_NAME_KEY = "STRING_NAME";
    public static final String TAG = "ResultsFragment";

    private RecyclerView resultRecyclerView;
    private ResultRecyclerAdapter resultAdapter;
    private TextView noResultText;
    private ImageView honeycombLoading;
    private AnimatedVectorDrawableCompat animation;

    private Parcelable recyclerViewState;

    private ResultType resultType = null;
    private String parameter = null;
    private String name = null;
    private String resultText = "";

    private boolean animationShown = false;

    private ImageView upButton;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        RepositoryViewModelFactory factory = ((MainActivity) requireActivity()).getAppContainer().repositoryViewModelFactory;
        ResultsViewModel viewModel = new ViewModelProvider(this, factory).get(ResultsViewModel.class);
        setViewModel(viewModel);

        View root = inflater.inflate(R.layout.fragment_results, container, false);
        noResultText = root.findViewById(R.id.no_result_text);
        resultRecyclerView = root.findViewById(R.id.vertical_search_result);
        noResultText.setVisibility(View.GONE);
        honeycombLoading = root.findViewById(R.id.honeycomb_loading_results);

        if (getArguments() != null) {
            resultType = ResultType.valueOf(getArguments().getString(RESULT_TYPE_KEY));
            parameter = getArguments().getString(STRING_PARAMETER_KEY);
            name = getArguments().getString(STRING_DISPLAYED_NAME_KEY);
            Log.d(TAG, "onCreateView: parameter obtained: " + resultType + "\t" + parameter);
        }

        if (name != null) {
            resultText = getResources().getString(R.string.result_for_label, name);
        }
        List<Game> games = viewModel.getGameResultLiveData(resultType, parameter).getValue();
        resultAdapter = new ResultRecyclerAdapter(requireContext(), resultText, games, getOnGameClickedListener(), getItemVisibleListener());
        resultRecyclerView.setAdapter(resultAdapter);
        RecyclerView.LayoutManager layoutManager = resultRecyclerView.getLayoutManager();
        if (layoutManager != null && recyclerViewState != null) {
            layoutManager.onRestoreInstanceState(recyclerViewState);
            recyclerViewState = null;
        }
        return root;
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        honeycombLoading.setVisibility(View.GONE);
        animation = AnimatedVectorDrawableCompat.create(requireContext(), R.drawable.honeycomb_loading_anim);
        honeycombLoading.setImageDrawable(animation);


        if (!animationShown) {
            setUpAnimation();
        }

        if (resultType != null && parameter != null) {
            viewModel.getGameResultLiveData(resultType, parameter).observe(
                    getViewLifecycleOwner(),
                    games -> {
                        long delay = 0;
                        if (!animationShown) {
                            stopAnimation();
                            delay = 200;
                        }
                        if (resultRecyclerView != null) {
                            resultRecyclerView.postDelayed(() -> {
                                final Context context = getContext();
                                if (context == null) {
                                    return;
                                }
                                if (games != null && games.size() != 0) {
                                    Log.d(TAG, "onChanged: List returned: games elements = " + games.size());
                                    resultAdapter.setList(games);
                                    resultAdapter.notifyDataSetChanged();
//                                    resultRecyclerView.setAdapter(new ResultRecyclerAdapter(
//                                            context,
//                                            resultText,
//                                            games,
//                                            getOnGameClickedListener(),
//                                            getItemVisibleListener()
//                                    ));
                                    RecyclerView.LayoutManager layoutManager = resultRecyclerView.getLayoutManager();
                                    if (layoutManager != null && recyclerViewState != null) {
                                        layoutManager.onRestoreInstanceState(recyclerViewState);
                                        recyclerViewState = null;
                                    }
                                    noResultText.setVisibility(View.GONE);
                                } else {
                                    Log.d(TAG, "onChanged: Empty list");
                                    noResultText.setVisibility(View.VISIBLE);
                                }
                            }, delay);
                        }
                    }
            );

            viewModel.getSearchDetailLiveData(resultType, parameter).observe(
                    getViewLifecycleOwner(),
                    searchDetail -> {
                        if (searchDetail != null) {
                            resultAdapter.setSearchDetail(searchDetail, new ResultRecyclerAdapter.OnSummaryClicked() {
                                @Override
                                public void onExpand() {
                                }

                                @Override
                                public void onCompress() {
                                    resultRecyclerView.smoothScrollToPosition(0);
                                }
                            });
                        }
                    });

        } else {
            Log.e(TAG, "onViewCreated: ERROR null parameter in ResultsFragment arguments");
        }
    }

    private ResultRecyclerAdapter.OnItemVisibleListener getItemVisibleListener() {
        return new ResultRecyclerAdapter.OnItemVisibleListener() {
            @Override
            public void onItemVisible() {
                viewModel.getMoreGameResults();
                //resultAdapter.notifyDataSetChanged();
            }

            @Override
            public int getPositionFromBottom() {
                return 5;
            }
        };
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!animationShown) {
            honeycombLoading.postDelayed(() -> animation.start(), 200);
        }
    }

    private void setUpAnimation() {
        honeycombLoading.setVisibility(View.VISIBLE);
        honeycombLoading.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (honeycombLoading.getWidth() != 0) {
                    honeycombLoading.setPivotX((honeycombLoading.getWidth() * 42.5f) / 100);
                    honeycombLoading.setPivotY((honeycombLoading.getWidth() * 50f) / 100);
                    honeycombLoading.setRotation(14);
                    honeycombLoading.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            }
        });
        animation.registerAnimationCallback(new Animatable2Compat.AnimationCallback() {
            @Override
            public void onAnimationEnd(Drawable drawable) {
                honeycombLoading.setRotation(honeycombLoading.getRotation() % 360 + 120f);
                animation.start();
            }
        });
    }

    private void stopAnimation() {
        animation.clearAnimationCallbacks();
        animation.registerAnimationCallback(new Animatable2Compat.AnimationCallback() {
            @Override
            public void onAnimationEnd(Drawable drawable) {
                animation.stop();
                honeycombLoading.animate().scaleX(0).scaleY(0).setInterpolator(new AccelerateInterpolator()).setDuration(180).start();
                honeycombLoading.animate().withEndAction(() -> honeycombLoading.setVisibility(View.GONE));
                animationShown = true;
                animation.clearAnimationCallbacks();
            }
        });
    }

    @Override
    public void onDestroyView() {
        resultRecyclerView.clearOnScrollListeners();
        RecyclerView.LayoutManager layoutManager = resultRecyclerView.getLayoutManager();
        if (layoutManager != null) {
            recyclerViewState = layoutManager.onSaveInstanceState();
        }
        super.onDestroyView();
    }
}