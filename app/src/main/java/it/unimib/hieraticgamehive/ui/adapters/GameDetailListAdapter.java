package it.unimib.hieraticgamehive.ui.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.NavOptions;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import it.unimib.hieraticgamehive.R;
import it.unimib.hieraticgamehive.ui.fragments.ResultsFragment;
import it.unimib.hieraticgamehive.utils.ResultType;

public abstract class GameDetailListAdapter<T> extends ListAdapter<T, GameDetailListAdapter.DetailViewHolder> {

    private final static String TAG = "GameDetailListAdapter";

    public enum AdapterType {
        TEXT,
        BUTTON,
        ;
    }

    private final Context context;
    private final AdapterType adapterType;
    private final ResultType resultType;

    public GameDetailListAdapter(Context context, @NonNull AdapterType adapterType, @NonNull ResultType resultType) {
        super(new DiffUtil.ItemCallback<T>() {
                  @Override
                  public boolean areItemsTheSame(@NonNull T oldItem, @NonNull T newItem) {
                      return oldItem.equals(newItem);
                  }

                  @SuppressLint("DiffUtilEquals")
                  @Override
                  public boolean areContentsTheSame(@NonNull T oldItem, @NonNull T newItem) {
                      return oldItem.equals(newItem);
                  }
              }
        );
        this.context = context;
        this.adapterType = adapterType;
        this.resultType = resultType;
    }

    public static class DetailViewHolder extends RecyclerView.ViewHolder {
        private final View view;
        private final AdapterType type;

        public DetailViewHolder(@NonNull View itemView, @NonNull AdapterType type) {
            super(itemView);
            if (type == AdapterType.TEXT) {
                this.view = itemView.findViewById(R.id.game_detail_value);
            } else if (type == AdapterType.BUTTON) {
                this.view = itemView.findViewById(R.id.genre_button);
                ((Button) view).setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
            } else {
                view = null;
            }
            this.type = type;
        }

        public void setText(String text) {
            if (text != null) {
                if (type == AdapterType.TEXT) {
                    ((TextView) view).setText(text);
                } else if (type == AdapterType.BUTTON) {
                    ((Button) view).setText(text);
                }
            }
        }

        public void setOnClick(@NonNull View.OnClickListener onClickListener) {
            view.setOnClickListener(onClickListener);
        }
    }

    @NonNull
    @Override
    public DetailViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view;
        if (adapterType == AdapterType.TEXT) {
            view = LayoutInflater.from(context).inflate(R.layout.single_detail_text_view, viewGroup, false);
        } else if (adapterType == AdapterType.BUTTON) {
            view = LayoutInflater.from(context).inflate(R.layout.single_genre_button, viewGroup, false);
        } else {
            view = new View(context);
        }
        return new DetailViewHolder(view, adapterType);
    }

    @Override
    public void onBindViewHolder(@NonNull DetailViewHolder holder, int position) {
        String value = getDisplayedValue(position);
        if (value != null && !value.isEmpty()) {
            if (adapterType == AdapterType.TEXT && position != getCurrentList().size() - 1) {
                value += ", ";
            }
            holder.setText(value);
        }
        String finalValue = value;
        holder.setOnClick(v -> {
            Log.d(TAG, "onBindViewHolder: onClick - Clicked on: " + finalValue);
            Bundle bundle = new Bundle();
            bundle.putString(ResultsFragment.RESULT_TYPE_KEY, resultType.name());
            bundle.putString(ResultsFragment.STRING_PARAMETER_KEY, getResultsParameterValue(position));
            bundle.putString(ResultsFragment.STRING_DISPLAYED_NAME_KEY, getDisplayedValue(position));
            Navigation.findNavController(v).navigate(R.id.fragment_results, bundle, new NavOptions.Builder()
                    .setEnterAnim(R.anim.slide_in)
                    .setExitAnim(android.R.anim.fade_out)
                    .setPopEnterAnim(android.R.anim.fade_in)
                    .setPopExitAnim(R.anim.slide_out)
                    .build());
        });
    }

    abstract String getDisplayedValue(int position);

    abstract String getResultsParameterValue(int position);
}