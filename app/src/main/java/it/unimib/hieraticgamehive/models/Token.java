package it.unimib.hieraticgamehive.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "token")
public class Token {
    @PrimaryKey(autoGenerate = true)
    public int roomId;

    @ColumnInfo(name = "access_token")
    @SerializedName("access_token")
    public String accessToken;

    @ColumnInfo(name = "expiration")
    @SerializedName("expiration")
    public long expiration;

    public Token() {
    }

    @Ignore
    public Token(String accessToken, int expiration) {
        this.accessToken = accessToken;
        this.expiration = expiration;
    }

    public boolean equals(Object object) {
        if(object instanceof Token) {
            Token other = (Token) object;
            return this.accessToken.equals(other.accessToken) && this.expiration == other.expiration;
        }
        return false;
    }
}
