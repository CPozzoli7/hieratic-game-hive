package it.unimib.hieraticgamehive.models;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.List;

import it.unimib.hieraticgamehive.BuildConfig;
import it.unimib.hieraticgamehive.utils.Highlights;
import proto.Game;

@Entity(tableName = "highlights")
public class CachedHighlightGameList {
    @Ignore
    private static final long EXPIRATION_TIME_DEFAULT = 10800; //3 hours

    @Ignore
    private static final long EXPIRATION_TIME_DEBUG = 0;

    @PrimaryKey
    @NonNull
    public Highlights highlight;

    @ColumnInfo(name = "game_list")
    public List<Game> gameList;

    @ColumnInfo(name = "expiration")
    public long expiration;

    public CachedHighlightGameList() {
    }

    @Ignore
    public CachedHighlightGameList(@NonNull Highlights highlight, @NonNull List<Game> gameList, long expirationSeconds) {
        this.highlight = highlight;
        this.gameList = gameList;
        expiration = System.currentTimeMillis() / 1000 + expirationSeconds;
    }

    @Ignore
    public CachedHighlightGameList(@NonNull Highlights highlight, @NonNull List<Game> gameList) {
        this(
                highlight,
                gameList,
                BuildConfig.DEBUG && EXPIRATION_TIME_DEBUG != 0 ? EXPIRATION_TIME_DEBUG : EXPIRATION_TIME_DEFAULT
        );
    }
}
