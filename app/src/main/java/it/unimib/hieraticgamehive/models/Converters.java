package it.unimib.hieraticgamehive.models;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import it.unimib.hieraticgamehive.GameDeserializer;
import it.unimib.hieraticgamehive.GameSerializer;
import it.unimib.hieraticgamehive.utils.Highlights;
import proto.Game;

public class Converters {
    @TypeConverter
    public static String fromHighlightEnum(Highlights highlight) {
        return highlight.name();
    }

    @TypeConverter
    public static Highlights fromHighlightEnum(String name) {
        return Highlights.valueOf(name);
    }

    @TypeConverter
    public static String fromGameList(List<Game> gameList) {
        Gson gson = new GsonBuilder().registerTypeAdapter(Game.class, new GameSerializer()).create();
        return gson.toJson(gameList);
    }

    @TypeConverter
    public static List<Game> toGameList(String json) {
        Gson gson = new GsonBuilder().registerTypeAdapter(Game.class, new GameDeserializer()).create();
        return gson.fromJson(
                json,
                new TypeToken<List<Game>>() {
                }.getType()
        );
    }
}
