package it.unimib.hieraticgamehive.repositories;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executor;

import it.unimib.hieraticgamehive.core.interfaces.OnCompleteCallback;
import it.unimib.hieraticgamehive.services.remote.UserService;

public class UserRepository {
    private final UserService userService;
    private final Executor executor;
    private List<Long> collection;
    private List<Long> wishlist;

    public UserRepository(UserService userService, Executor executor) {
        this.userService = userService;
        this.executor = executor;
        if (isLogged()) {
            updateIds();
        }
        FirebaseAuth.getInstance().addAuthStateListener(
                firebaseAuth -> {
                    if (isLogged()) {
                        updateIds();
                    } else {
                        collection = null;
                        wishlist = null;
                    }
                });
    }

    public void addUserListener(FirebaseAuth.AuthStateListener authStateListener) {
        FirebaseAuth.getInstance().addAuthStateListener(authStateListener);
    }

    public boolean isLogged() {
        return userService.getUser() != null;
    }


    private boolean isFetchingCollection = false;
    private final List<OnCompleteCallback<List<Long>>> collectionCallbacks = new ArrayList<>();

    private synchronized void fetchCollection(OnCompleteCallback<List<Long>> onCompleteCallback) {
        if (onCompleteCallback != null) {
            collectionCallbacks.add(onCompleteCallback);
        }
        if (isFetchingCollection) {
            return;
        }
        isFetchingCollection = true;
        userService.getGameIds(
                executor,
                UserService.UserEndpoint.COLLECTION,
                new OnCompleteCallback<List<Long>>() {
                    @Override
                    public void onComplete(List<Long> result) {
                        isFetchingCollection = false;
                        collection = Collections.synchronizedList(result);
                        completeCollectionCallbacks(result);
                    }

                    @Override
                    public void onError() {
                        isFetchingCollection = false;
                        completeCollectionCallbacks(collection);
                    }
                }
        );
    }

    private boolean isFetchingWishlist = false;
    private final List<OnCompleteCallback<List<Long>>> wishlistCallbacks = new ArrayList<>();

    private synchronized void fetchWishlist(OnCompleteCallback<List<Long>> onCompleteCallback) {
        if (onCompleteCallback != null) {
            wishlistCallbacks.add(onCompleteCallback);
        }
        if (isFetchingWishlist) {
            return;
        }
        isFetchingWishlist = true;
        userService.getGameIds(
                executor,
                UserService.UserEndpoint.WISHLIST,
                new OnCompleteCallback<List<Long>>() {
                    @Override
                    public void onComplete(List<Long> result) {
                        isFetchingWishlist = false;
                        wishlist = Collections.synchronizedList(result);
                        completeWishlistCallbacks(result);
                    }

                    @Override
                    public void onError() {
                        isFetchingWishlist = false;
                        completeWishlistCallbacks(wishlist);
                    }
                }
        );
    }

    private void completeCollectionCallbacks(List<Long> result) {
        completeCallbacks(collectionCallbacks, result);
    }

    private void completeWishlistCallbacks(List<Long> result) {
        completeCallbacks(wishlistCallbacks, result);
    }

    private void completeCallbacks(List<OnCompleteCallback<List<Long>>> onCompleteCallbacks, List<Long> result) {
        for (int i = 0; i < onCompleteCallbacks.size(); i++) {
            onCompleteCallbacks.get(i).onComplete(result);
        }
        onCompleteCallbacks.clear();
    }


    private void updateIds() {
        if (!isLogged()) {
            return;
        }
        fetchCollection(null);
        fetchWishlist(null);
    }

    public boolean addToCollection(long id) {
        return add(id, collection, UserService.UserEndpoint.COLLECTION);
    }

    public boolean addToWishlist(long id) {
        return add(id, wishlist, UserService.UserEndpoint.WISHLIST);
    }

    private boolean add(long id, List<Long> list, UserService.UserEndpoint userEndpoint) {
        if (!isLogged()) {
            return false;
        }
        boolean added = list != null && list.add(id);
        if (added) {
            userService.addGameId(id, userEndpoint);
        }
        return added;
    }

    public boolean removeFromCollection(long id) {
        return remove(id, collection, UserService.UserEndpoint.COLLECTION);
    }

    public boolean removeFromWishlist(long id) {
        return remove(id, wishlist, UserService.UserEndpoint.WISHLIST);
    }

    private boolean remove(long id, List<Long> list, UserService.UserEndpoint userEndpoint) {
        if (!isLogged()) {
            return false;
        }
        boolean removed = list != null && list.remove(id);
        if (removed) {
            userService.removeGameId(id, userEndpoint);
        }
        return removed;
    }

    public boolean isInCollection(long id) {
        return isInList(id, collection, UserService.UserEndpoint.COLLECTION);
    }

    public boolean isInWishlist(long id) {
        return isInList(id, wishlist, UserService.UserEndpoint.WISHLIST);
    }

    private boolean isInList(long id, List<Long> list, UserService.UserEndpoint userEndpoint) {
        return isLogged() && list != null && list.contains(id);
    }

    public void getCollection(OnCompleteCallback<List<Long>> onCompleteCallback) {
        if (collection != null) {
            onCompleteCallback.onComplete(collection);
            return;
        }
        executor.execute(() -> fetchCollection(onCompleteCallback));
    }

    public void getWishlist(OnCompleteCallback<List<Long>> onCompleteCallback) {
        if (wishlist != null) {
            onCompleteCallback.onComplete(wishlist);
            return;
        }
        executor.execute(() -> fetchWishlist(onCompleteCallback));
    }
}