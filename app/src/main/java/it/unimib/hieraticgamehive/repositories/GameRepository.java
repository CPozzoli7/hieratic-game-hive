package it.unimib.hieraticgamehive.repositories;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import it.unimib.hieraticgamehive.GameList;
import it.unimib.hieraticgamehive.GameHashMap;
import it.unimib.hieraticgamehive.HighlightGameList;
import it.unimib.hieraticgamehive.core.MainDatabase;
import it.unimib.hieraticgamehive.core.interfaces.OnCompleteCallback;
import it.unimib.hieraticgamehive.models.CachedHighlightGameList;
import it.unimib.hieraticgamehive.services.remote.IGDBService;
import it.unimib.hieraticgamehive.utils.Genres;
import it.unimib.hieraticgamehive.utils.Highlights;
import it.unimib.hieraticgamehive.utils.IGDBFields;
import it.unimib.hieraticgamehive.utils.ResultType;
import it.unimib.hieraticgamehive.utils.SearchDetail;
import it.unimib.hieraticgamehive.utils.Themes;
import proto.Company;
import proto.Game;
import proto.Platform;
import proto.PlatformVersion;

public class GameRepository {
    private static final String TAG = "GameRepository";
    private final IGDBService igdbService;
    private final MainDatabase database;
    private final GameHashMap gameHashMap = new GameHashMap(); //TODO Remove GameHashMap
    private final Map<Integer, List<Game>> highlightsList; //TODO Consider change into SparseArray
    private GameList resultsList;
    private OnCompleteCallback<Map<Integer, List<Game>>> highlightsOnCompleteCallback;

    private final Executor executor;

    public GameRepository(IGDBService igdbService, MainDatabase mainDatabase, Executor executor) {
        this.igdbService = igdbService;
        this.database = mainDatabase;
        this.executor = executor;
        highlightsList = new ConcurrentHashMap<>();
    }


    public void getHighlights(OnCompleteCallback<Map<Integer, List<Game>>> callback) { //TODO Dependency injection of executor (CachedThreadPool)
        highlightsOnCompleteCallback = callback;
        if (highlightsList.size() != 0) {
            callback.onComplete(highlightsList);
            return;
        }
        Highlights[] highlights = Highlights.values();
        for (Highlights highlightValue : highlights) {
            executor.execute(() -> {
                CachedHighlightGameList cachedList = database.getHighlightsDAO().findByHighlight(highlightValue);
                if (cachedList == null || cachedList.expiration <= System.currentTimeMillis() / 1000) {
                    List<Game> gameList;
                    gameList = requestHighlight(highlightValue);
                    Log.d(TAG, "getHighlights: Completed " + highlightValue.name() + " remote request");
                    if (gameList != null && gameList.size() > 0) {
                        database.getHighlightsDAO().insert(new CachedHighlightGameList(highlightValue, gameList));
                        updateHighlightsList(highlightValue, gameList);
                    } else {
                        updateHighlightsList(highlightValue, Collections.emptyList());
                    }
                } else {
                    updateHighlightsList(highlightValue, cachedList.gameList);
                }
            });
            //onCompleteHighlightsCallback.onComplete(highlightsList);
        }
    }

    public List<Game> requestHighlight(Highlights highlight) {
        Log.d(TAG, "requestHighlight: Remote request for " + highlight.name());
        switch (highlight) {
            case MOST_POPULAR:
                return igdbService.getMostPopular(IGDBFields.COVER);
            case MOST_ANTICIPATED:
                return igdbService.getMostAnticipated(IGDBFields.COVER);
            case SUGGESTED:
                return igdbService.getSuggested(IGDBFields.COVER);
            case LATEST_RELEASES:
                return igdbService.getLatestReleases(IGDBFields.COVER);
            case RETROS:
                return igdbService.getRetros(IGDBFields.COVER);
            case BEST_RATED:
                return igdbService.getBestRated(IGDBFields.COVER);
            case WORST_RATED:
                return igdbService.getWorstRated(IGDBFields.COVER);
            default:
                return null;
        }
    }

    private int highlightsCount = 0;
    private final Object lock = new Object();

    private void updateHighlightsList(Highlights highlight, List<Game> gameList) {
        if (!(gameList instanceof GameList)) {
            gameList = new HighlightGameList(gameHashMap, gameList, highlight);
        }
        synchronized (lock) {
            highlightsCount++;
        }
        highlightsList.put(highlight.ordinal(), gameList);
        if (highlightsCount == Highlights.values().length) {
            highlightsOnCompleteCallback.onComplete(highlightsList);
        }
    }

    public void getGameResults(ResultType type, String parameter, int offset, OnCompleteCallback<List<Game>> onCompleteCallback) {
        Executors.newSingleThreadExecutor().execute(() -> {
            Log.d(TAG, "getGameResults: Request for: " + type + " - " + parameter);
            List<Game> gameList = null;
            switch (type) {
                case GENRE:
                    gameList = igdbService.getGamesByGenre(
                            IGDBFields.COVER_DETAIL, offset, Genres.valueOf(parameter)
                    );
                    break;
                case THEME:
                    gameList = igdbService.getGamesByTheme(
                            IGDBFields.COVER_DETAIL, offset, Themes.valueOf(parameter)
                    );
                    break;
                case SEARCH:
                    gameList = igdbService.searchGames(
                            IGDBFields.COVER_DETAIL, offset, parameter
                    );
                    break;
                case COMPANY:
                    gameList = igdbService.getGamesByCompany(
                            IGDBFields.COVER_DETAIL, offset, Long.parseLong(parameter)
                    );
                    break;
                case YEAR:
                    gameList = igdbService.getGamesByYear(
                            IGDBFields.COVER_DETAIL, offset, Long.parseLong(parameter)
                    );
                    break;
                case PLATFORM:
                    gameList = igdbService.getGamesByPlatform(
                            IGDBFields.COVER_DETAIL, offset, Long.parseLong(parameter)
                    );
                    break;
            }
            if (gameList == null) {
                onCompleteCallback.onError();
                return;
            }
//            if (resultsList != null) {
//                resultsList.clear();
//            }
//            resultsList = new GameList(gameHashMap, gameList);
            onCompleteCallback.onComplete(gameList);
            Log.d(TAG, "getGameResults: Request completed for: " + type + " - " + parameter);
        });
    }

    public void getSearchDetail(ResultType resultType, String parameter, OnCompleteCallback<SearchDetail> onCompleteCallback) {
        Executors.newSingleThreadExecutor().execute(() -> {
            final SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy", Locale.getDefault());
            switch (resultType) {
                case PLATFORM:
                    Platform platform = igdbService.getPlatform(Long.parseLong(parameter));
                    if (platform == null) {
                        return;
                    }
                    String platformName = platform.getName();
                    String platformReleaseDateYear = "";
                    String platformImageId = platform.getPlatformLogoOrBuilder().getImageId();
                    String platformSummary = platform.getSummary();
                    if (platform.getVersionsCount() > 0) {
                        PlatformVersion lastPlatformVersion = platform.getVersions(platform.getVersionsCount() - 1);
                        if (platformImageId.isEmpty()) {
                            platformImageId = lastPlatformVersion.getPlatformLogo().getImageId();
                        }
                        if (lastPlatformVersion.getPlatformVersionReleaseDatesCount() > 0) {
                            long platformReleaseDateSeconds = lastPlatformVersion.getPlatformVersionReleaseDates(0).getDate().getSeconds();
                            if (platformReleaseDateSeconds > 0) {
                                Date date = new java.util.Date(platformReleaseDateSeconds * 1000L);
                                platformReleaseDateYear = sdf.format(date);
                            }
                        }
                        if (platformSummary.isEmpty()) {
                            platformSummary = lastPlatformVersion.getSummary();
                        }
                    }
                    onCompleteCallback.onComplete(new SearchDetail(
                            platformName,
                            platformImageId,
                            platformReleaseDateYear,
                            platformSummary
                    ));
                    return;
                case COMPANY:
                    Company company = igdbService.getCompany(Long.parseLong(parameter));
                    if (company == null) {
                        return;
                    }
                    String companyName = company.getName();
                    long companyReleaseDateSeconds = company.getStartDate().getSeconds();
                    String companyReleaseYear = "";
                    if (companyReleaseDateSeconds > 0) {
                        Date date = new java.util.Date(companyReleaseDateSeconds * 1000L);
                        companyReleaseYear = sdf.format(date);
                    }
                    String companyImageId = company.getLogoOrBuilder().getImageId();
                    String description = company.getDescription();
                    onCompleteCallback.onComplete(new SearchDetail(
                            companyName,
                            companyImageId,
                            companyReleaseYear,
                            description
                    ));
                    return;
                default:
                    onCompleteCallback.onError();
            }
        });
    }

    private Game selectedGame = null;

    public void setSelectedGame(Game selectedGame) {
        this.selectedGame = selectedGame;
    }

    public Game getGame(long gameId, OnCompleteCallback<Game> onCompleteUpdateCallback) {
        Executors.newSingleThreadExecutor().execute(() -> {
            Game game;
            game = igdbService.getGame(gameId, IGDBFields.DETAIL);
            if (game != null) {
                onCompleteUpdateCallback.onComplete(game);
            } else {
                onCompleteUpdateCallback.onError();
            }
        });
        if (selectedGame != null && selectedGame.getId() == gameId) {
            return selectedGame;
        }
        return null;
    }

    public Game getGame(String gamePath, OnCompleteCallback<Game> onCompleteUpdateCallback) {
        Executors.newSingleThreadExecutor().execute(() -> {
            Game game;
            game = igdbService.getGame(gamePath, IGDBFields.DETAIL);
            if (game != null) {
                onCompleteUpdateCallback.onComplete(game);
            } else {
                onCompleteUpdateCallback.onError();
            }
        });
        return null;
    }

    public void getSimilarGames
            (List<Game> gameList, OnCompleteCallback<List<Game>> onCompleteCallback) {
        executor.execute(() -> {
            List<Game> result = igdbService.getCompleteMultipleGames(gameList, IGDBFields.COVER);
            if (result != null) {
                onCompleteCallback.onComplete(result);
            } else {
                onCompleteCallback.onError();
            }
        });
    }

    public void getMultipleGames
            (List<Long> gameList, OnCompleteCallback<List<Game>> onCompleteCallback) {
        executor.execute(() -> {
            List<Game> result = igdbService.getMultipleGames(gameList, IGDBFields.COVER);
            if (result != null) {
                onCompleteCallback.onComplete(result);
            } else {
                onCompleteCallback.onComplete(Collections.emptyList());
            }
        });
    }
}