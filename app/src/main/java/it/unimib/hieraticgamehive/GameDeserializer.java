package it.unimib.hieraticgamehive;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.protobuf.Timestamp;

import java.lang.reflect.Type;

import proto.Company;
import proto.Cover;
import proto.Game;
import proto.InvolvedCompany;

import static it.unimib.hieraticgamehive.GameSerializer.*;

public class GameDeserializer implements JsonDeserializer<Game> {
    @Override
    public Game deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Game.Builder gameBuilder = Game.newBuilder();
        JsonObject jsonObject = json.getAsJsonObject();
        gameBuilder.setId(jsonObject.get(GAME_ID).getAsLong());
        gameBuilder.setName(jsonObject.get(GAME_NAME).getAsString());
        JsonElement cover = jsonObject.get(GAME_COVER_ID);
        if (cover != null) {
            gameBuilder.setCover(Cover.newBuilder().setImageId(cover.getAsString()).build());
        }
        gameBuilder.setSummary(jsonObject.get(GAME_SUMMARY).getAsString());
        JsonElement firstReleaseDate = jsonObject.get(GAME_FIRST_RELEASE_DATE);
        if (firstReleaseDate != null) {
            gameBuilder.setFirstReleaseDate(Timestamp.newBuilder().setSeconds(firstReleaseDate.getAsLong()).build());
        }
        JsonElement jsonElement = jsonObject.get(GAME_COMPANIES);
        if (jsonElement != null && jsonElement.isJsonArray()) {
            JsonArray companies = jsonElement.getAsJsonArray();
            for (int i = 0; i < companies.size(); i++) {
                JsonObject singleCompany = companies.get(i).getAsJsonObject();
                long id = singleCompany.get(COMPANY_ID).getAsLong();
                boolean isDeveloper = singleCompany.get(COMPANY_IS_DEVELOPER).getAsBoolean();
                boolean isPublisher = singleCompany.get(COMPANY_IS_PUBLISHER).getAsBoolean();
                String name = singleCompany.get(COMPANY_NAME).getAsString();
                gameBuilder.addInvolvedCompanies(InvolvedCompany.newBuilder().setDeveloper(isDeveloper).setPublisher(isPublisher).setCompany(Company.newBuilder().setName(name).setId(id).build()).build());
            }
        }
        return gameBuilder.build();
    }
}
