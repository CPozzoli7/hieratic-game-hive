package it.unimib.hieraticgamehive;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Objects;
import java.util.RandomAccess;

import proto.Game;

public class GameList implements List<Game>, RandomAccess {
    private final static String TAG = "GameList";
    private final HashMap<Long, Game> gameHashMap;
    private final ArrayList<Long> keyList = new ArrayList<>();

    public GameList(GameHashMap gameHashMap, @NonNull List<Game> gameList) {
        this.gameHashMap = gameHashMap;
        for (int i = 0; i < gameList.size(); i++) {
            Game game = gameList.get(i);
            keyList.add(i, game.getId());
            gameHashMap.put(game.getId(), game);
        }
    }

    @Override
    public int size() {
        return keyList.size();
    }

    @Override
    public boolean isEmpty() {
        return gameHashMap.isEmpty();
    }

    @Override
    public boolean contains(@Nullable Object object) {
        if (object != null && object.getClass().isAssignableFrom(Game.class)) {
            return gameHashMap.containsKey(((Game) object).getId());
        } else if (object.getClass().isAssignableFrom(Long.TYPE)) {
            return gameHashMap.containsKey(object);
        }
        return false;
    }

    @NonNull
    @Override
    public Iterator iterator() {
        return new GameIterator(keyList.iterator());
    }

    private class GameIterator implements Iterator<Game> {
        private final Iterator<Long> iterator;

        public GameIterator(Iterator<Long> keyListIterator) {
            this.iterator = keyListIterator;
        }

        @Override
        public boolean hasNext() {
            return iterator.hasNext();
        }

        @Override
        public Game next() {
            return gameHashMap.get(iterator.next());
        }
    }

    @NonNull
    @Override
    public Game[] toArray() {
        Game[] gameArray = new Game[keyList.size()];
        return toArray(gameArray);
    }

    @NonNull
    @Override
    public <T> T[] toArray(@NonNull T[] objects) {
        Game[] array;
        if (objects.length >= gameHashMap.size()) {
            array = (Game[]) objects;
        } else {
            array = new Game[gameHashMap.size()];
        }
        for (int i = 0; i < keyList.size(); i++) {
            array[i] = gameHashMap.get(keyList.get(i));
        }
        return (T[]) array;
    }

    @Override
    public boolean add(Game game) {
        keyList.add(game.getId());
        gameHashMap.put(game.getId(), game);
        return true;
    }

    public boolean remove(@Nullable Long key, @Nullable Game value) {
        keyList.remove(key);
        return gameHashMap.remove(key, value);
    }

    @Override
    public boolean remove(@Nullable Object object) {
        if (object != null && object.getClass().isAssignableFrom(Game.class)) {
            return remove(((Game) object).getId()) != null;
        }
        return false;
    }


    @Override
    public Game remove(int i) {
        return remove(keyList.get(i));
    }

    public Game remove(@Nullable Long key) {
        keyList.remove(key);
        return gameHashMap.remove(key);
    }

    @Override
    public boolean addAll(@NonNull Collection collection) {
        return false;
    }

    @Override
    public boolean addAll(int i, @NonNull Collection collection) {
        return false;
    }

    @Override
    public boolean retainAll(@NonNull Collection collection) {
        return false;
    }

    @Override
    public boolean removeAll(@NonNull Collection collection) {
        return false;
    }

    @Override
    public boolean containsAll(@NonNull Collection collection) {
        return false;
    }

    public void clear() {
        for (int i = 0; i < keyList.size(); i++) {
            gameHashMap.remove(keyList.get(i));
        }
        keyList.clear();
    }

    @Override
    public Game get(int index) {
        if(keyList.size() <= index) {
            return null;
        }
        return gameHashMap.get(keyList.get(index));
    }

    public Game get(long key) {
        return get(Long.valueOf(key));
    }

    public Game get(Long key) {
        return gameHashMap.get(key);
    }

    @Override
    public Game set(int i, Game game) {
        if (game == null) {
            return null;
        }
        Game prev = gameHashMap.get(keyList.get(i));
        add(game);
        return prev;
    }

    @Override
    public void add(int i, Game game) {
        if (game == null) {
            return;
        }
        keyList.add(i, game.getId());
        gameHashMap.put(game.getId(), game);
    }

    @Override
    public int lastIndexOf(@Nullable Object object) {
        return indexOf(object);
    }

    @Override
    public int indexOf(@Nullable Object object) {
        if (object == null || !object.getClass().isAssignableFrom(Game.class)) {
            return -1;
        }
        return indexOf(((Game) object).getId());
    }

    public int indexOf(Long key) {
        return keyList.indexOf(key);
    }

    @NonNull
    @Override
    public ListIterator listIterator() {
        return null;
    }

    @NonNull
    @Override
    public ListIterator listIterator(int i) {
        return null;
    }

    @NonNull
    @Override
    public List subList(int i, int i1) {
        keyList.subList(i, i1);
        return null;
    }
}