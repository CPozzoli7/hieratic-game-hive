package it.unimib.hieraticgamehive;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.protobuf.Timestamp;

import java.lang.reflect.Type;

import proto.Cover;
import proto.Game;
import proto.InvolvedCompany;

public class GameSerializer implements JsonSerializer<Game> {
    public static final String GAME_ID = "id";
    public static final String GAME_NAME = "name";
    public static final String GAME_COVER_ID = "cover_id";
    public static final String GAME_SUMMARY = "summary";
    public static final String GAME_FIRST_RELEASE_DATE = "first_release_date";
    public static final String GAME_COMPANIES = "companies";
    public static final String COMPANY_NAME = "company_name";
    public static final String COMPANY_IS_DEVELOPER = "company_developer";
    public static final String COMPANY_IS_PUBLISHER = "company_publisher";
    public static final String COMPANY_ID = "company_id";

    @Override
    public JsonElement serialize(Game game, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject json = new JsonObject();
        json.addProperty(GAME_ID, game.getId());
        json.addProperty(GAME_NAME, game.getName());
        Cover cover = game.getCover();
        if (cover != null) {
            json.addProperty(GAME_COVER_ID, cover.getImageId());
        }
        json.addProperty(GAME_SUMMARY, game.getSummary());
        Timestamp firstReleaseDate = game.getFirstReleaseDate();
        if (firstReleaseDate != null) {
            json.addProperty(GAME_FIRST_RELEASE_DATE, firstReleaseDate.getSeconds());
        }
        JsonArray companies = new JsonArray();
        for (int i = 0; i < game.getInvolvedCompaniesCount(); i++) {
            InvolvedCompany involvedCompany = game.getInvolvedCompanies(i);
            if (involvedCompany != null && involvedCompany.getCompany() != null) {
                if (involvedCompany.getDeveloper() || involvedCompany.getPublisher()) {
                    JsonObject singleDeveloper = new JsonObject();
                    singleDeveloper.addProperty(COMPANY_ID, involvedCompany.getCompany().getId());
                    singleDeveloper.addProperty(COMPANY_IS_DEVELOPER, involvedCompany.getDeveloper());
                    singleDeveloper.addProperty(COMPANY_IS_PUBLISHER, involvedCompany.getPublisher());
                    singleDeveloper.addProperty(COMPANY_NAME, involvedCompany.getCompany().getName());
                    companies.add(singleDeveloper);
                }
            }
        }
        if (companies.size() > 0) {
            json.add(GAME_COMPANIES, companies);
        }
        return json;
    }
}
