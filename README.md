# Hieratic Game Hive

This is a University project for 'Mobile Development' course exam.  
Hieratic Game Hive is an Android app developed in Java built around IGDB database.

# Description

It offers a simple UI with which you can navigate through the IGDB database and view games' details with information like release date, publishers, developers, screenshots, etc.  
You can also sign-in using your email or Google account and save your favourite games in your collection or wishlist.

# Libraries

- [IGDB API-JVM](https://github.com/husnjak/IGDB-API-JVM)
- Firebase ([Authentication](https://firebase.google.com/products/auth), [Realtime Database](https://firebase.google.com/products/realtime-database), [Crashlytics](https://firebase.google.com/products/crashlytics))
- [Glide](https://github.com/bumptech/glide)
- [Room](https://developer.android.com/jetpack/androidx/releases/room)

# Screenshots

| Home | Search | Results | Profile | Detail |
| ------ | ------ | ------ | ------ | ------ |
| <img src="https://gitlab.com/CPozzoli7/hieratic-game-hive/-/raw/dev/doc/home.png" width="250"> | <img src="https://gitlab.com/CPozzoli7/hieratic-game-hive/-/raw/dev/doc/search.png" width="250"> | <img src="https://gitlab.com/CPozzoli7/hieratic-game-hive/-/raw/dev/doc/results.png" width="250"> | <img src="https://gitlab.com/CPozzoli7/hieratic-game-hive/-/raw/dev/doc/profile.png" width="250"> | <img src="https://gitlab.com/CPozzoli7/hieratic-game-hive/-/raw/dev/doc/detail.gif" width="250"> |
